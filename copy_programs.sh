# This script copies programs from a checkout of the sv-benchmarks repository
# (path to which needs to be given as argument) into the witnesses subfolder
# of the corresponding witness. Intended to facilitate benchmarking, copied
# programs are ignored by git and should not be checked in.

if [ $# -eq 0 ];
then echo "Specify source directory"; exit 1;
else source=$1;
fi

dest=witnesses;
count=0
for witness in $(find $dest -type f -name '*.yml'); do
    program_name=$(basename $witness .yml)
    echo Looking for $program_name...
    program=$(find $source -type f -name "$program_name")
    new_name="$(dirname $witness)/$(basename $program)"
    count=$(($count + 1))
    cp $program $new_name
done
echo Copied $count programs

