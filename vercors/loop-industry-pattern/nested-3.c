extern int __VERIFIER_nondet_int();

//VERIFICATION RESULT : TRUE

int main() {
	int last = __VERIFIER_nondet_int();
	int a=0,b=0,c=0,st=0;
	//@ loop_invariant a == b;
	while(1 == 1) {
		st=1;  
		//@ loop_invariant a == b && c <= 200000 && (last != 200000 || st == 1);
		for(c=0;c<200000;c++)
			if (c==last ) st = 0; 
		if(st==0 && c==last+1){
			a+=3; b+=3;}
		else {	a+=2; b+=2; } 
		if(c==last && st==0) 
			a = a+1;
		//@ assert a == b && c == 200000;
	}
}
