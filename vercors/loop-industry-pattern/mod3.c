// Category: Loops
// Verification result: TRUE
extern int __VERIFIER_nondet_int();

int main(){
  unsigned int x = __VERIFIER_nondet_int();
  unsigned int y = 1;
  
  //@ loop_invariant x % 3 == 0 || y != 0;
  while(__VERIFIER_nondet_int() != 0){
    if(x % 3 == 1){
      x += 2; y = 0;}
    else{
      if(x % 3 == 2){
	x += 1; y = 0;}
      else{
	if(__VERIFIER_nondet_int() != 0){
	  x += 4; y = 1;}
	else{
	  x += 5; y = 1;}
      }
    }
  }
  if(y == 0) {
    //@ assert x % 3 == 0;
  }
  return 0;
}

  
