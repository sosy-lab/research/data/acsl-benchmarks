extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  if (!(y>0 || x>0)) return 0;
  //@ loop_invariant x > 0 || y > 0;
  while (1 == 1) {
    if (x+y>-2) {
      break;
    }
    if (x>0) {
      x++;
    } else {
      y++;
    }
  }
  //@ assert x > 0 || y > 0;
  return 0;
}
