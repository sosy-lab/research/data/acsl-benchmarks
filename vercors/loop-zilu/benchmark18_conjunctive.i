extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int i = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  if (!((i==0) && (k==0) && (n>0))) return 0;
  //@ loop_invariant i == k && k <= n;
  while (i < n) {
    i++;k++;
  }
  //@ assert i == k && k == n;
  return 0;
}
