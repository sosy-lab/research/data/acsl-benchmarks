extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 15.cfg:
names=low mid high
precondition=low == 0 && mid >= 1 && high == 2*mid
loopcondition=mid > 0
loop=low = low + 1; high = high - 1; mid = mid - 1;
postcondition=low == high
learners= conj
*/
int main() {
  int low = __VERIFIER_nondet_int();
  int mid = __VERIFIER_nondet_int();
  int high = __VERIFIER_nondet_int();
  if (!(low == 0 && mid >= 1 && high == 2*mid)) return 0;
  //@ loop_invariant mid >= 0 && low + 2 * mid == high;
  while (mid > 0) {
    low = low + 1;
    high = high - 1;
    mid = mid - 1;
  }
  //@ assert low == high;
  return 0;
}
