extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  if (!(x == y && y >=0)) return 0;
  //@ loop_invariant 0 <= x && x == y;
  while (x!=0) {
    x--;
    y--;
    if (x<0 || y<0) break;
  }
  //@ assert y == 0;
  return 0;
}
