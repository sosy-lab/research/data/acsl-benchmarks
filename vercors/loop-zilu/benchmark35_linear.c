extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 35.cfg:
names=x
precondition=x>=0
loopcondition=(x>=0) && (x<10) 
loop= x=x+1;
postcondition=x>=10
learners= linear
*/
int main() {
  int x = __VERIFIER_nondet_int();
  if (!(x>=0)) return 0;
  //@ loop_invariant x >= 0;
  while ((x>=0) && (x<10)) {
    x=x+1;
  }
  //@ assert x >= 10;
  return 0;
}
