extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 34.cfg:
names=j k n
precondition= (j==0) && (k==n) && (n>0)
loopcondition=j<n && n>0
loop=j++;k--;
postcondition= (k == 0)
learners= conj
*/
int main() {
  int j = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  if (!((j==0) && (k==n) && (n>0))) return 0;
  //@ loop_invariant k + j == n && n > 0 && j <= n;
  while (j<n && n>0) {
    j++;k--;
  }
  //@ assert k == 0;
  return 0;
}
