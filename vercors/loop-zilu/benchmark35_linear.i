extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int x = __VERIFIER_nondet_int();
  if (!(x>=0)) return 0;
  //@ loop_invariant x >= 0;
  while ((x>=0) && (x<10)) {
    x=x+1;
  }
  //@ assert x >= 10;
  return 0;
}
