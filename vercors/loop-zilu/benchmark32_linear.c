extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 32.cfg:
names= x
beforeloop=
beforeloopinit=
precondition=x==1 || x==2
loopcondition=
loop=if(x==1) x=2; else if (x==2) x=1;
postcondition=x<=8
afterloop=
learners= linear
*/
int main() {
  int x = __VERIFIER_nondet_int();
  
  if (!(x==1 || x==2)) return 0;
  //@ loop_invariant x == 1 || x == 2;
  while (__VERIFIER_nondet_bool()) {
    if(x==1) x=2;
    else if (x==2) x=1;
  }
  //@ assert x <= 8;
  return 0;
}
