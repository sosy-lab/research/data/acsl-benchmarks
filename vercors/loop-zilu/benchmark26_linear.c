extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 26.cfg:
names=x y
precondition=x<y
loopcondition=x<y
loop= x=x+1;
postcondition=x==y
learners=linear
*/
int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  if (!(x<y)) return 0;
  //@ loop_invariant x <= y;
  while (x<y) {
    x=x+1;
  }
  //@ assert x == y;
  return 0;
}
