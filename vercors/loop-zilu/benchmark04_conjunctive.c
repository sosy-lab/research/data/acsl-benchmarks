extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 04.cfg:
names=k j n
beforeloop=
beforeloopinit=
precondition=n>=1 && k>=n && j==0
loopcondition=j<=n-1
loop=j++; k--;
postcondition=k>=0
afterloop=
learners= conj
*/
int main() {
  int k = __VERIFIER_nondet_int();
  int j = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  
  if (!(n>=1 && k>=n && j==0)) return 0;
  //@ loop_invariant j <= n && n <= k + j;
  while (j<=n-1) {
    j++;
    k--;
  }
  //@ assert k >= 0;
  return 0;
}
