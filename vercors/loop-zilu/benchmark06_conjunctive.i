extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int j = __VERIFIER_nondet_int();
  int i = __VERIFIER_nondet_int();
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  j=0;
  if (!(x+y==k)) return 0;
  //@ loop_invariant x + y == k;
  while (__VERIFIER_nondet_bool()) {
    if(j==i) {x++;y--;} else {y++;x--;} j++;
  }
  //@ assert x + y == k;
  return 0;
}
