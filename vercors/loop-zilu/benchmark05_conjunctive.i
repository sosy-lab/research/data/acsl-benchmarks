extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();
int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  if (!(x>=0 && x<=y && y<n)) return 0;
  //@ loop_invariant x <= y && y <= n;
  while (x<n) {
    x++;
    if (x>y) y++;
  }
  //@ assert y == n;
  return 0;
}
