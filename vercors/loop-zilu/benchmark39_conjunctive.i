extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  if (!(x == 4*y && x >= 0)) return 0;
  //@ loop_invariant 0 <= x && x == 4 * y;
  while (x > 0) {
    x-=4;
    y--;
  }
  //@ assert y >= 0;
  return 0;
}
