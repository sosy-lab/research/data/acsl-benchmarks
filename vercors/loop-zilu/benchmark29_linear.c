extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 29.cfg:
names= x y
precondition= x<y
loopcondition= x<y
loop= x=x+100;
postcondition= x >= y && x <= y + 99
learners=linear
*/
int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  if (!(x<y)) return 0;
  //@ loop_invariant x <= y + 99;
  while (x<y) {
    x=x+100;
  }
  //@ assert x >= y && x <= y + 99;
  return 0;
}
