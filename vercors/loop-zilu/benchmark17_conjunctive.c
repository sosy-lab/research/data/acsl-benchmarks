extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();


/* 17.cfg:
names=i k n
beforeloop=
beforeloopinit=
precondition=i==0 && k==0
loopcondition=i<n
loop=i++; k++;
postcondition=k>=n
afterloop=
learners= conj
*/
int main() {
  int i = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  
  if (!(i==0 && k==0)) return 0;
  //@ loop_invariant k == i && i >= 0 && (i <= n || n < 0);
  while (i<n) {
    i++;
    k++;
  }
  //@ assert k >= n;
  return 0;
}
