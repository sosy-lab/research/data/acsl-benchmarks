extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 25.cfg:
names=x
precondition=x<0
loopcondition=x<10 
loop= x=x+1;
postcondition=x==10
learners=linear
*/
int main() {
  int x = __VERIFIER_nondet_int();
  if (!(x<0)) return 0;
  //@ loop_invariant x <= 10;
  while (x<10) {
    x=x+1;
  }
  //@ assert x == 10;
  return 0;
}
