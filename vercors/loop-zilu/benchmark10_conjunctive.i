extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int i = __VERIFIER_nondet_int();
  int c = __VERIFIER_nondet_int();
  if (!(c==0 && i==0)) return 0;
  //@ loop_invariant 2 * c == (i - 1) * i && 0 <= i && i <= 100;
  while (i<100) {
    c=c+i;
    i=i+1;
    if (i<=0) break;
  }
  //@ assert c >= 0;
  return 0;
}
