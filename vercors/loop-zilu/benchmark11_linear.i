extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int x = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  if (!(x==0 && n>0)) return 0;
  //@ loop_invariant x <= n;
  while (x<n) {
    x++;
  }
  //@ assert x == n;
  return 0;
}
