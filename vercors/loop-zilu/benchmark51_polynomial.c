extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 51.cfg:
names= x
precondition=(x>=0) && (x<=50)
beforeloop=
beforeloopinit=
loopcondition=
loop= if (x>50) x++; if (x == 0) { x ++; } else x--;
postcondition=(x>=0) && (x<=50)
learners=poly
*/
int main() {
  int x = __VERIFIER_nondet_int();
  
  if (!((x>=0) && (x<=50))) return 0;
  //@ loop_invariant 0 <= x && x <= 50;
  while (__VERIFIER_nondet_bool()) {
    if (x>50) x++;
    if (x == 0) { x ++;
    } else x--;
  }
  //@ assert x >= 0 && x <= 50;
  return 0;
}
