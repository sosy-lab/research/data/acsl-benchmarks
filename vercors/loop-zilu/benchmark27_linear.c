extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 27.cfg:
names=i j k
precondition=i<j && k>i-j
loopcondition=i<j
loop=k=k+1; i=i+1;
postcondition=k > 0
learners=linear
*/
int main() {
  int i = __VERIFIER_nondet_int();
  int j = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  if (!(i<j && k>i-j)) return 0;
  //@ loop_invariant k > i - j && i <= j;
  while (i<j) {
    k=k+1;
    i=i+1;
  }
  //@ assert k > 0;
  return 0;
}
