extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 14.cfg:
names=i
beforeloop=
beforeloopinit=
precondition=i>=0 && i<=200
loopcondition=i>0
loop=i--;
postcondition=i>=0
afterloop=
learners= linear
*/
int main() {
  int i = __VERIFIER_nondet_int();
  
  if (!(i>=0 && i<=200)) return 0;
  //@ loop_invariant i >= 0;
  while (i>0) {
    i--;
  }
  //@ assert i >= 0;
  return 0;
}
