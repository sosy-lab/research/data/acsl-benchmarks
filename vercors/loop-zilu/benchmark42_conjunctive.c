extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 42.cfg:
names=x y z
precondition=x == y && x >= 0 && x+y+z==0
loopcondition=x > 0
loop=x--; y--; z++; z++;
postcondition=z<=0
learners=conj
*/
int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int z = __VERIFIER_nondet_int();
  if (!(x == y && x >= 0 && x+y+z==0)) return 0;
  //@ loop_invariant x == y && y >= 0 && x + y + z == 0;
  while (x > 0) {
    x--;
    y--;
    z++;
    z++;
  }
  //@ assert z <= 0;
  return 0;
}
