extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 11.cfg:
names=x n
beforeloop=
beforeloopinit=
precondition=x==0 && n>0
loopcondition=x<n
loop=x++;
postcondition=x==n
afterloop=
learners= linear
*/
int main() {
  int x = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  
  if (!(x==0 && n>0)) return 0;
  //@ loop_invariant x <= n;
  while (x<n) {
    x++;
  }
  //@ assert x == n;
  return 0;
}
