extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int i = __VERIFIER_nondet_int();
  int j = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  if (!(i==0 && j==0)) return 0;
  //@ loop_invariant j == i;
  while (i <= k) {
    i++;
    j=j+1;
  }
  //@ assert j == i;
  return 0;
}
