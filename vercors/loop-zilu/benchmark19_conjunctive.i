extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int j = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  if (!((j==n) && (k==n) && (n>0))) return 0;
  //@ loop_invariant k == j && j >= 0 && n > 0;
  while (j>0 && n>0) {
    j--;k--;
  }
  //@ assert k == 0;
  return 0;
}
