extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int i = __VERIFIER_nondet_int();
  int j = __VERIFIER_nondet_int();
  int r = __VERIFIER_nondet_int();
  if (!(r > i + j)) return 0;
  //@ loop_invariant r > i + j;
  while (i > 0) {
    i = i - 1;
    j = j + 1;
  }
  //@ assert r > i + j;
  return 0;
}
