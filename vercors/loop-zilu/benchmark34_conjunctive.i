extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int j = __VERIFIER_nondet_int();
  int k = __VERIFIER_nondet_int();
  int n = __VERIFIER_nondet_int();
  if (!((j==0) && (k==n) && (n>0))) return 0;
  //@ loop_invariant k + j == n && n > 0 && j <= n;
  while (j<n && n>0) {
    j++;k--;
  }
  //@ assert k == 0;
  return 0;
}
