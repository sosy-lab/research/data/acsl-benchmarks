extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

/* 37.cfg:
names=x y
precondition=x == y && x >= 0
loopcondition=x > 0
loop=x--; y--;
postcondition=y>=0
learners=conj
*/
int main() {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  if (!(x == y && x >= 0)) return 0;
  //@ loop_invariant y == x && x >= 0;
  while (x > 0) {
    x--;
    y--;
  }
  //@ assert y >= 0;
  return 0;
}
