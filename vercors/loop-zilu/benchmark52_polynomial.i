extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();

int main() {
  int i = __VERIFIER_nondet_int();
  if (!(i < 10 && i > -10)) return 0;
  //@ loop_invariant -10 < i && i <= 10;
  while (i * i < 100) {
    i = i + 1;
  }
  //@ assert i == 10;
  return 0;
}
