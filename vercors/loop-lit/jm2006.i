int __VERIFIER_nondet_int();
int main() {
    int i, j;
    i = __VERIFIER_nondet_int();
    j = __VERIFIER_nondet_int();
    if (!(i >= 0 && j >= 0)) return 0;
    int x = i;
    int y = j;
    //@ loop_invariant x - y == i - j;
    while(x != 0) {
        x--;
        y--;
    }
    if (i == j) {
        //@ assert y == 0;
    }
    return 0;
}
