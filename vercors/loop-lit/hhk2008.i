int __VERIFIER_nondet_int();
int main() {
    int a = __VERIFIER_nondet_int();
    int b = __VERIFIER_nondet_int();
    int res, cnt;
    if (!(a <= 1000000)) return 0;
    if (!(0 <= b && b <= 1000000)) return 0;
    res = a;
    cnt = b;
    //@ loop_invariant res + cnt == a + b && cnt >= 0;
    while (cnt > 0) {
 cnt = cnt - 1;
 res = res + 1;
    }
    //@ assert res == a + b;
    return 0;
}
