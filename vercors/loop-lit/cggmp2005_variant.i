int __VERIFIER_nondet_int();
int main() {
    int lo, mid, hi;
    lo = 0;
    mid = __VERIFIER_nondet_int();
    if (!(mid > 0 && mid <= 1000000)) return 0;
    hi = 2*mid;
    //@ loop_invariant 2*mid + lo == hi && mid >= 0;
    while (mid > 0) {
        lo = lo + 1;
        hi = hi - 1;
        mid = mid - 1;
    }
    //@ assert lo == hi;
    return 0;
}
