int __VERIFIER_nondet_int();
int main() {
    int i,j;
    i = 1;
    j = 10;
    //@ loop_invariant j == 10 - (i - 1)/2 && 6 <= j && j <= 10 && 1 <= i && i <= 9 && j % 3 == i % 3;
    while (j >= i) {
        i = i + 2;
        j = -1 + j;
    }
    //@ assert j == 6;
    return 0;
}
