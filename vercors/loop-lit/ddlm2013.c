// Source: Isil Dillig, Thomas Dillig, Boyang Li, Ken McMillan: "Inductive
// Invariant Generation via Abductive Inference", OOPSLA 2013.

#include "assert.h"

int main() {
    unsigned int i,j,a,b;
    int flag = __VERIFIER_nondet_int();
    a = 0;
    b = 0;
    j = 1;
    if (flag != 0) {
        i = 0;
    } else {
        i = 1;
    }

    //@ loop_invariant (i % 2 == 0 && j - i == 1 && a == b) || flag == 0;
    while (__VERIFIER_nondet_int() != 0) {
        a++;
        b += (j - i);
        i += 2;
        if (i%2 == 0) {
            j += 2;
        } else {
            j++;
        }
    }
    if (flag != 0) {
        //@ assert a == b;
    }
    return 0;
}
