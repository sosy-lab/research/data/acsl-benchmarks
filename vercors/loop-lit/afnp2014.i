int __VERIFIER_nondet_int();
int main() {
    int x = 1;
    int y = 0;
    //@ loop_invariant (x >= y && y >= 1 || (x == 1 && y == 0)) && y <= 1000;
    while (y < 1000 && __VERIFIER_nondet_int() != 0) {
        x = x + y;
        y = y + 1;
    }
    //@ assert x >= y;
    return 0;
}
