int __VERIFIER_nondet_int();
int main() {
    int x = 0;
    int m = 0;
    int n = __VERIFIER_nondet_int();
    //@ loop_invariant (0 <= m && m < x && x <= n) || (m == x && x == 0) || (n <= 0);
    while(x < n) {
 if(__VERIFIER_nondet_int() != 0) {
     m = x;
 }
 x = x + 1;
    }
    //@ assert m >= 0 || n <= 0;
    //@ assert m < n || n <= 0;
    return 0;
}
