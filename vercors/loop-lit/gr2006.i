int __VERIFIER_nondet_int();
int main() {
    int x,y;
    x = 0;
    y = 0;
    //@ loop_invariant (0 <= x && x == y && y <= 50) || (x >= 50 && x + y == 100 && y >= 0);
    while (1 == 1) {
        if (x < 50) {
            y++;
        } else {
            y--;
        }
        if (y < 0) break;
        x++;
    }
    //@ assert x == 100;
    return 0;
}
