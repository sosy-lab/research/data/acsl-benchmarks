// Source: Sumit Gulwani, Saurabh Srivastava, Ramarathnam Venkatesan: "Program
// Analysis as Constraint Solving", PLDI 2008.

#include "assert.h"
int main() {
    int x,y;
    x = -50;
    y = __VERIFIER_nondet_int();
    if (!(-1000 < y && y < LARGE_INT)) return 0;
    //@ loop_invariant y <= x && x < 0 || x < y || y > 0;
    while (x < 0) {
	x = x + y;
	y++;
    }
    //@ assert y > 0;
    return 0;
}
