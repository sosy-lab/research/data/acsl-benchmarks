int __VERIFIER_nondet_int();
int main() {
    int x = 0;
    int y = 50;
    //@ loop_invariant (x < y && y == 50 || x == y && y >= 50) && x <= 100;
    while(x < 100) {
 if (x < 50) {
     x = x + 1;
 } else {
     x = x + 1;
     y = y + 1;
 }
    }
    //@ assert y == 100;
    return 0;
}
