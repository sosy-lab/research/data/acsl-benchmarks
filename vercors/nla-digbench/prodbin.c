/* shift_add algorithm for computing the 
   product of two natural numbers
*/
extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    int a, b;
    int x, y, z;

    a = __VERIFIER_nondet_int();
    b = __VERIFIER_nondet_int();
    assume_abort_if_not(b >= 1);

    x = a;
    y = b;
    z = 0;

    //@ loop_invariant z + x * y == a * b && y >= 0;
    while (1 == 1) {
    	//@ assert z + x * y == a * b;
        if (!(y != 0))
            break;

        if (y % 2 == 1) {
            z = z + x;
            y = y - 1;
        }
        x = 2 * x;
        y = y / 2;
    }
    //@ assert z == a * b;
    
    return 0;
}
