/*
  Bresenham's line drawing algorithm 
  from Srivastava et al.'s paper From Program Verification to Program Synthesis in POPL '10 
*/
extern int __VERIFIER_nondet_int();

int main() {
    int X, Y;
    long long x, y, v, xy, yx;
    X = __VERIFIER_nondet_int();
    Y = __VERIFIER_nondet_int();
    v = ((long long) 2 * Y) - X;         // cast required to avoid int overflow
    y = 0;
    x = 0;

    //@ loop_invariant 2 * Y * x - 2 * X * y - X + 2 * Y - v == 0 && (x <= X + 1 || (X < 0 && 0 == x && v == 2 * Y - X));
    while (1 == 1) {
        yx = (long long) Y*x;
        xy = (long long) X*y;
        //@ assert 2 * yx - 2 * xy - X + 2 * Y - v == 0;
        if (!(x <= X))
            break;
        // out[x] = y

        if (v < 0) {
            v = v + (long long) 2 * Y;
        } else {
            v = v + 2 * ((long long) Y - X);
            y++;
        }
        x++;
    }
    xy = (long long) x*y;
    yx = (long long) Y*x;
    //@ assert 2 * yx - 2 * xy - X + 2 * Y - v + 2 * y == 0;

    return 0;
}
