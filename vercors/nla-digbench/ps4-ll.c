extern short __VERIFIER_nondet_short();

int main() {
    short k;
    long long y, x, c;
    k = __VERIFIER_nondet_short();

    y = 0;
    x = 0;
    c = 0;

    //@ loop_invariant 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0 && c == y && (c <= k || k < 0 && 0 == c);
    while (1 == 1) {
        //@ assert 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0;

        if (!(c < k))
            break;

        c = c + 1;
        y = y + 1;
        x = y * y * y + x;
    }
    //@ assert k * y - y * y == 0;
    //@ assert 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0;
    return 0;
}
