extern unsigned __VERIFIER_nondet_uint();

int main() {
  unsigned A, B;
  unsigned q, r, b;
    A = __VERIFIER_nondet_uint();
    B = 1;
    q = 0;
    r = A;
    b = B;
    //@ loop_invariant B == 1 && q == 0 && r == A && (b == 1 || b % 2 == 0);
    while (1 == 1) {
        if (!(r >= b)) break;
        b = 2 * b;
    }
    //@ loop_invariant A == q * b + r && (b == 1 || b % 2 == 0);
    while (1 == 1) {
    	//@ assert A == q * b + r;
        if (!(b != B)) break;
        q = 2 * q;
        b = b / 2;
        if (r >= b) {
            q = q + 1;
            r = r - b;
        }
    }
    //@ assert A == q * b + r;
    return 0;
}
