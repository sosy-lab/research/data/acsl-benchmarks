extern int __VERIFIER_nondet_int();

int main() {
    int k;
    long long y, x, c;
    k = __VERIFIER_nondet_int();

    y = 0;
    x = 0;
    c = 0;

    //@ loop_invariant y * y - 2 * x + y == 0;
    while (1 == 1) {
        //@ assert y * y - 2 * x + y == 0;

        if (!(c < k))
            break;

        c = c + 1;
        y = y + 1;
        x = y + x;
    }
    //@ assert y * y - 2 * x + y == 0;
     
    return 0;
}
