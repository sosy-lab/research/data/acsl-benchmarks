/* extended Euclid's algorithm */
extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}


int main() {
    int x, y;
    int a, b, p, q, r, s, c, k;
    x = __VERIFIER_nondet_int();
    y = __VERIFIER_nondet_int();
    assume_abort_if_not(x >= 1);
    assume_abort_if_not(y >= 1);

    a = x;
    b = y;
    p = 1;
    q = 0;
    r = 0;
    s = 1;
    c = 0;
    k = 0;
    //@ loop_invariant a == y * r + x * p && b == x * q + y * s && q * x * y + s * y * y - q * x - b * y - s * y + b == 0;
    while (1 == 1) {
        if (!(b != 0))
            break;
        c = a;
        k = 0;

        //@ loop_invariant k * b + c == a && a == y * r + x * p && b == x * q + y * s && q * x * y + s * y * y - q * x - b * y - s * y + b == 0;
        while (1 == 1) {
            //@ assert a == k * b + c;
            //@ assert a == y * r + x * p;
            //@ assert b == x * q + y * s;
            //@ assert q * x * y + s * y * y - q * x - b * y - s * y + b == 0;
            if (!(c >= b))
                break;
            c = c - b;
            k = k + 1;
        }

        a = b;
        b = c;

        int temp;
        temp = p;
        p = q;
        q = temp - q * k;
        temp = r;
        r = s;
        s = temp - s * k;
    }
    

    //@ assert q * x + s * y == 0;
    //@ assert p * x + r * y == a;
    return a;
}
