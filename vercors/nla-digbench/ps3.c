extern int __VERIFIER_nondet_int();

int main() {
    int k, y, x, c;
    k = __VERIFIER_nondet_int();

    y = 0;
    x = 0;
    c = 0;

    //@ loop_invariant 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
    while (1 == 1) {
        //@ assert 6 * x - 2 * y * y * y - 3 * y * y - y == 0;

        if (!(c < k))
            break;

        c = c + 1;
        y = y + 1;
        x = y * y + x;
    }
    //@ assert 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
    return 0;
}
