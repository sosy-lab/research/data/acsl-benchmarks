/* extended Euclid's algorithm */
extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    int x, y;
    long long a, b, p, q, r, s;
    x = __VERIFIER_nondet_int();
    y = __VERIFIER_nondet_int();
    assume_abort_if_not(x >= 1);
    assume_abort_if_not(y >= 1);

    a = x;
    b = y;
    p = 1;
    q = 0;
    r = 0;
    s = 1;

    //@ loop_invariant a == y * r + x * p && b == x * q + y * s;
    while (1 == 1) {
        if (!(b != 0))
            break;
        long long c, k;
        c = a;
        k = 0;

        //@ loop_invariant k * b + c == a && a == y * r + x * p && b == x * q + y * s;
        while (1 == 1) {
            if (!(c >= b))
                break;
            long long d, v;
            d = 1;
            v = b;

            //@ loop_invariant k * b + c == a && a == y * r + x * p && b == x * q + y * s && v == b * d;
            while (1 == 1) {
                //@ assert a == y * r + x * p;
                //@ assert b == x * q + y * s;
                //@ assert a == k * b + c;
                //@ assert v == b * d;

                if (!(c >= 2 * v))
                    break;
                d = 2 * d;
                v = 2 * v;
            }
            c = c - v;
            k = k + d;
        }

        a = b;
        b = c;
        long long temp;
        temp = p;
        p = q;
        q = temp - q * k;
        temp = r;
        r = s;
        s = temp - s * k;
    }
    //@ assert p * x - q * x + r * y - s * y == a;
    return 0;
}
