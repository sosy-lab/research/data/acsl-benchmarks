/* Algorithm for computing simultaneously the GCD and the LCM, by Dijkstra */

extern unsigned __VERIFIER_nondet_uint();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    unsigned a, b;
    unsigned x, y, u, v;
    a = __VERIFIER_nondet_uint();
    b = __VERIFIER_nondet_uint();
    assume_abort_if_not(a >= 1); //inf loop if remove
    assume_abort_if_not(b >= 1);

    assume_abort_if_not(a <= 65535);
    assume_abort_if_not(b <= 65535);

    x = a;
    y = b;
    u = b;
    v = a;

    //@ loop_invariant x * u + y * v == 2 * a * b;
    while (1 == 1) {
        //@ assert x * u + y * v == 2 * a * b;

        if (!(x != y))
            break;

        if (x > y) {
            x = x - y;
            v = v + u;
        } else {
            y = y - x;
            u = u + v;
        }
    }

    //@ assert x * u + y * v == 2 * a * b;
    // x == gcd(a,b)
    //(u + v)/2==lcm(a,b)

    return 0;
}
