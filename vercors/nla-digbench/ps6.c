extern int __VERIFIER_nondet_int();

int main() {
    int k, y, x, c;
    k = __VERIFIER_nondet_int();

    y = 0;
    x = 0;
    c = 0;

    //@ loop_invariant -2 * y * y * y * y * y * y - 6 * y * y * y * y * y - 5 * y * y * y * y + y * y + 12 * x == 0 && c == y && (c <= k || k < 0 && 0 == c);
    while (1 == 1) {
        //@ assert -2 * y * y * y * y * y * y - 6 * y * y * y * y * y - 5 * y * y * y * y + y * y + 12 * x == 0;

        if (!(c < k))
            break;

        c = c + 1;
        y = y + 1;
        x = y * y * y * y * y + x;
    }
    
    //@ assert -2 * y * y * y * y * y * y - 6 * y * y * y * y * y -5 * y * y * y * y + y * y + 12 * x == 0;
    //@ assert k * y == y * y;	      
    return 0;
}
