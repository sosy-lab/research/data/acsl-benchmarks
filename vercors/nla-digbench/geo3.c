/* 
Geometric Series
computes x = sum(z^k)[k=0..k-1], y = z^(k-1)
*/

extern int __VERIFIER_nondet_int();

int main() {
    int z, a, k;
    int x, y, c;
    z = __VERIFIER_nondet_int();
    a = __VERIFIER_nondet_int();
    k = __VERIFIER_nondet_int();

    x = a;
    y = 1;
    c = 1;

    //@ loop_invariant z * x - x + a - a * z * y == 0;
    while (1 == 1) {
        //@ assert z * x - x + a - a * z * y == 0;

        if (!(c < k))
            break;

        c = c + 1;
        x = x * z + a;
        y = y * z;
    }
    //@ assert z * x - x + a - a * z * y == 0;
    return x;
}
