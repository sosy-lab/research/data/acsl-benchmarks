/* extended Euclid's algorithm */
extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    int a, b, p, q, r, s;
    int x, y;
    x = __VERIFIER_nondet_int();
    y = __VERIFIER_nondet_int();
    assume_abort_if_not(x >= 1);
    assume_abort_if_not(y >= 1);

    a = x;
    b = y;
    p = 1;
    q = 0;
    r = 0;
    s = 1;

    //@ loop_invariant p * s - r * q == 1 && a == y * r + x * p && b == x * q + y * s;
    while (1 == 1) {
        //@ assert 1 == p * s - r * q;
        //@ assert a == y * r + x * p;
        //@ assert b == x * q + y * s;

        if (!(a != b))
            break;

        if (a > b) {
            a = a - b;
            p = p - q;
            r = r - s;
        } else {
            b = b - a;
            q = q - p;
            s = s - r;
        }
    }
    
    //@ assert a - b == 0;
    //@ assert p * x + r * y - b == 0;    
    //@ assert q * r - p * s + 1 == 0;
    //@ assert q * x + s * y - b == 0;
    return 0;
}
