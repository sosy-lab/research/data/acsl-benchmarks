/*
  hardware integer division program, by Manna
  returns q==A//B
  */

extern int __VERIFIER_nondet_int();

int main() {
    int A, B;
    int r, d, p, q;
    A = __VERIFIER_nondet_int();
    B = 1;

    r = A;
    d = B;
    p = 1;
    q = 0;

    //@ loop_invariant q == 0 && r == A && d == B * p;
    while (1 == 1) {
        //@ assert q == 0;
        //@ assert r == A;
        //@ assert d == B * p;
        if (!(r >= d)) break;

        d = 2 * d;
        p = 2 * p;
    }

    //@ loop_invariant A == q * B + r && d == B * p;
    while (1 == 1) {
        //@ assert A == q * B + r;
        //@ assert d == B * p;

        if (!(p != 1)) break;

        d = d / 2;
        p = p / 2;
        if (r >= d) {
            r = r - d;
            q = q + p;
        }
    }

    //@ assert A == d * q + r;
    //@ assert B == d;
    return 0;
}
