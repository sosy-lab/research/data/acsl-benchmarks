/*
 * algorithm for computing simultaneously the GCD and the LCM,
 * by Sankaranarayanan
 */

extern unsigned __VERIFIER_nondet_uint();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    unsigned a, b;
    unsigned x, y, u, v;
    a = __VERIFIER_nondet_uint();
    b = __VERIFIER_nondet_uint();
    assume_abort_if_not(a >= 1);  //infinite loop if remove
    assume_abort_if_not(b >= 1);

    assume_abort_if_not(a <= 65535);
    assume_abort_if_not(b <= 65535);

    x = a;
    y = b;
    u = b;
    v = 0;

    //@ loop_invariant x * u + y * v == a * b;
    while (1 == 1) {
        //@ assert x * u + y * v == a * b;
        if (!(x != y))
            break;

        //@ loop_invariant x * u + y * v == a * b;
        while (1 == 1) {
	    //@ assert x * u + y * v == a * b;
            if (!(x > y))
                break;
            x = x - y;
            v = v + u;
        }

        //@ loop_invariant x * u + y * v == a * b;
        while (1 == 1) {
	    //@ assert x * u + y * v == a * b;
            if (!(x < y))
                break;
            y = y - x;
            u = u + v;
        }
    }

    //@ assert u * y + v * y == a * b;
    //@ assert x == y;

    //x == gcd(a,b)
    //u + v == lcm(a,b)
    return 0;
}
