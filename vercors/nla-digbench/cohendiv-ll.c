/*
  Cohen's integer division
  returns x % y
  http://www.cs.upc.edu/~erodri/webpage/polynomial_invariants/cohendiv.htm
*/
extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    int x, y;
    long long q, r, a, b;

    x = __VERIFIER_nondet_int();
    y = __VERIFIER_nondet_int();

    //@ assume y >= 1;
    assume_abort_if_not(y >= 1);

    q = 0;
    r = x;
    a = 0;
    b = 0;

    //@ loop_invariant b == y * a && x == q * y + r && y >= 1;
    while (1 == 1) {
        //@ assert b == y * a;
	//@ assert x == q * y + r;
    
	if (!(r >= y))
	    break;
	a = 1;
	b = y;

	//@ loop_invariant b == y * a && x == q * y + r && r >= 0 && y >= 1;
	while (1 == 1) {            
	    //@ assert b == y * a;
	    //@ assert x == q * y + r;
	    //@ assert r >= 0;

	    if (!(r >= 2 * b))
		break;
	    
	    //@ assert r >= 2 * y * a;
	    
	    a = 2 * a;
	    b = 2 * b;
	}
	r = r - b;
	q = q + a;
    }
    
    //@ assert x == q * y + r;
    return 0;
}
