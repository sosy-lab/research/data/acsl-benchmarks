/* algorithm for computing the product of two natural numbers */

extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    int x, y;
    long long a, b, p, q;

    x = __VERIFIER_nondet_int();
    y = __VERIFIER_nondet_int();
    assume_abort_if_not(y >= 1);

    a = x;
    b = y;
    p = 1;
    q = 0;

    //@ loop_invariant q + a * b * p == x * y;
    while (1 == 1) {
        //@ assert q + a * b * p == x * y;

        if (!(a != 0 && b != 0))
            break;

        if (a % 2 == 0 && b % 2 == 0) {
            a = a / 2;
            b = b / 2;
            p = 4 * p;
        } else if (a % 2 == 1 && b % 2 == 0) {
            a = a - 1;
            q = q + b * p;
        } else if (a % 2 == 0 && b % 2 == 1) {
            b = b - 1;
            q = q + a * p;
        } else {
            a = a - 1;
            b = b - 1;
            q = q + (a + b + 1) * p; /*fix a bug here---  was (a+b-1)*/
        }
    }

    //@ assert q == x * y;
    //@ assert a * b == 0;
    return 0;
}
