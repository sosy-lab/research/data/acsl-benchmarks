/* program computing a divisor for factorisation, by Bressoud */

extern int __VERIFIER_nondet_int();
extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}

int main() {
    int A, R;
    long long u, v, r;
    A = __VERIFIER_nondet_int();
    R = __VERIFIER_nondet_int();
    //assume_abort_if_not(A >= 1);
    assume_abort_if_not(((long long) R - 1) * ((long long) R - 1) < A);
    //assume_abort_if_not(A <= R * R);
    assume_abort_if_not(A % 2 == 1);

    u = ((long long) 2 * R) + 1;
    v = 1;
    r = ((long long) R * R) - A;

    //@ loop_invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
    while (1 == 1) {
        //@ assert 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
        if (!(r != 0)) break;

        if (r > 0) {
            r = r - v;
            v = v + 2;
        } else {
            r = r + u;
            u = u + 2;
        }
    }

    //return  (u - v) / 2;
    //@ assert 4 * A == u * u - v * v - 2 * u + 2 * v;
    return 0;
}
