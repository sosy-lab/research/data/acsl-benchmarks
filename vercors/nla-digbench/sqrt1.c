/* Compute the floor of the square root of a natural number */

extern int __VERIFIER_nondet_int();


int main() {
    int n, a, s, t;
    n = __VERIFIER_nondet_int();

    a = 0;
    s = 1;
    t = 1;

    //@ loop_invariant t == 2 * a + 1 && s == (a + 1) * (a + 1) && t * t - 4 * s + 2 * t + 1 == 0;
    while (1 == 1) {
        //@ assert t == 2 * a + 1;
        //@ assert s == (a + 1) * (a + 1);
        //@ assert t * t - 4 * s + 2 * t + 1 == 0;
        // the above 2 should be equiv to 

        if (!(s <= n))
            break;

        a = a + 1;
        t = t + 2;
        s = s + t;
    }
    
    //@ assert t == 2 * a + 1;
    //@ assert s == (a + 1) * (a + 1);
    //@ assert t * t - 4 * s + 2 * t + 1 == 0;

    return 0;
}
