/*
  Bresenham's line drawing algorithm 
  from Srivastava et al.'s paper From Program Verification to Program Synthesis in POPL '10 
*/
extern int __VERIFIER_nondet_int();

int main() {
    int X, Y;
    int v, x, y;
    X = __VERIFIER_nondet_int();
    Y = __VERIFIER_nondet_int();
    v = 2 * Y - X;
    y = 0;
    x = 0;

    //@ loop_invariant 2 * Y * x - 2 * X * y - X + 2 * Y - v == 0 && (x <= X + 1 || (X < 0 && 0 == x && v == 2 * Y - X));
    while (1 == 1) {
    	//@ assert 2 * Y * x - 2 * X * y - X + 2 * Y - v == 0;
        if (!(x <= X))
            break;
        // out[x] = y

        if (v < 0) {
            v = v + 2 * Y;
        } else {
            v = v + 2 * (Y - X);
            y++;
        }
        x++;
    }
    //@ assert 2 * Y * x - 2 * x * y - X + 2 * Y - v + 2 * y == 0;

    return 0;
}
