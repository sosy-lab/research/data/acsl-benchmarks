# Benchmarks for Vercors

Benchmark programs with ACSL annotations translated into the specification language used by VerCors (https://github.com/utwente-fmt/vercors).

VerCors does not support all parts of the C11 syntax, so some additional changes were made to the programs, most notably:

 - Removal of __VERIFIER_assert / reach_error / abort calls and declarations as far as possible without altering the program semantics.
 - Replacement of integer expressions with boolean ones where necessary.
 - Removal of programs with unsupported features that are central to the verification task.
 - Conversion of global variables to local variables if possible (as global variables are "temporarily unsupported" by VerCors).
 - Inlining of #define pragmas where necessary

Known limitations of VerCors:
 - No implicit casts between integer types (at least int/_Bool/char)
 - No support for explicit casts
 - No support for global variables
 - No support for structs
 - No support for hexadecimal numbers
 - No support for sizeof
 - Limited support for array initializers
 - Limited support for unsigned types
 - Limited support for labels
 
## Verification

Compared to the corresponding ACSL annotations only syntactic changes were made.
Thus, not all programs can necessarily be verified with VerCors.
However, most benchmarks can still be verified as-is with the default options of VerCors, invoked via
```
[path-to-vercors-dir]/bin/vct [program]
```
