/* Benchmark used to verify Chimdyalwar, Bharti, et al. "VeriAbs: Verification by abstraction (competition contribution)." 
International Conference on Tools and Algorithms for the Construction and Analysis of Systems. Springer, Berlin, Heidelberg, 2017.*/

int main()
{
  unsigned int i = 0;
  unsigned int j = 0;
  unsigned int k = 0;

  //@ loop_invariant k == 3 * i && j == 2 * i;
  while (k < 268435455) {
    i = i + 1;
    j = j + 2;
    k = k + 3;
    //@ assert k == 3 * i && j == 2 * i;
  }

}
