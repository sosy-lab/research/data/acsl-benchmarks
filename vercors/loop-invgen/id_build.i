int __VERIFIER_nondet_int();
int main() {
  int offset, length, nlen = __VERIFIER_nondet_int();
  int i, j;
  //@ loop_invariant i >= 0;
  for (i=0; i<nlen; i++) {
    //@ loop_invariant 0 <= nlen - 1 - i && nlen - 1 - i < nlen;
    for (j=0; j<8; j++) {
      //@ assert 0 <= nlen - 1 - i;
      //@ assert nlen - 1 - i < nlen;
    }
  }
  return 0;
}
