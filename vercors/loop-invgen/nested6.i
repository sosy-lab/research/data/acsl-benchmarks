int __VERIFIER_nondet_int();
int main() {
    int i,j,k,n;
    k = __VERIFIER_nondet_int();
    n = __VERIFIER_nondet_int();
    if (!(n < 1000000)) return 0;
    if( k == n) {
    } else {
        goto END;
    }
    //@ loop_invariant k == n;
    for (i=0;i<n;i++) {
        //@ loop_invariant k == n && j >= 2 * i;
        for (j=2*i;j<n;j++) {
            if( __VERIFIER_nondet_int() != 0) {
                //@ loop_invariant 2 * i <= j && j <= k && k <= n;
                for (k=j;k<n;k++) {
                    //@ assert k >= 2 * i;
                }
            }
            else {
                //@ assert k >= n;
                //@ assert k <= n;
            }
        }
    }
END:
  return 0;
}
