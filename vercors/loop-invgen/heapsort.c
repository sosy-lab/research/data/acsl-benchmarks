#include "assert.h"

int main( int argc, char *argv[]){
  int n,l,r,i,j;

  n = __VERIFIER_nondet_int();
  if (!(1 <= n && n <= LARGE_INT)) return 0;


  l = n/2 + 1;
  r = n;
  if(l>1) {
    l--;
  } else {
    r--;
  }
  //@ loop_invariant 1 <= l && l <= r && r <= n || r == 0;
  while(r > 1) {
    i = l;
    j = 2*l;
    //@ loop_invariant 1 <= j && 1 <= i && i <= r && 1 <= l && l <= r && r <= n;
    while(j <= r) {
      if( j < r) {
        //@ assert 1 <= j;
	//@ assert j <= n;
	//@ assert 1 <= j + 1;
	//@ assert j + 1 <= n;
	if( __VERIFIER_nondet_int() != 0)
	  j = j + 1;
      }
      //@ assert 1 <= j;
      //@ assert j <= n;
      if( __VERIFIER_nondet_int() != 0) { 
      	break;
      }
      //@ assert 1 <= i;
      //@ assert i <= n;
      //@ assert 1 <= j;
      //@ assert j <= n;
      i = j;
      j = 2*j;
    }
    if(l > 1) {
      //@ assert 1 <= l;
      //@ assert l <= n;
      l--;
    } else {
      //@ assert 1 <= r;
      //@ assert r <= n;
      r--;
    }
  }
  return 0;
}

