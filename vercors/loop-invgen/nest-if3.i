int __VERIFIER_nondet_int();
int main() {
  int i,k,n,l;
  n = __VERIFIER_nondet_int();
  l = __VERIFIER_nondet_int();
  if (!(l>0)) return 0;
  if (!(l < 1000000)) return 0;
  if (!(n < 1000000)) return 0;
  //@ loop_invariant n <= k && k == 1 || (1 <= l && l < 1000000 + k && 1 <= k && k <= n && n < 1000000);
  for (k=1;k<n;k++){
    //@ loop_invariant 1 <= l && l < 1000000 + k && 1 <= k && k < n && n < 1000000 && 1 <= l && l <= i && (i <= n || l > n);
    for (i=l;i<n;i++){
      //@ assert 1 <= i;
    }
    if(__VERIFIER_nondet_int() != 0)
      l = l + 1;
  }
 }
