#include "assert.h"
int main()
{
  int tagbuf_len;
  int t;
  
  tagbuf_len = __VERIFIER_nondet_int();
  if(tagbuf_len >= 1); else goto END;

  t = 0;

  --tagbuf_len;

  //@ loop_invariant 0 <= t && t <= tagbuf_len;
  while (1 == 1) {
    if (t == tagbuf_len) {
      //@ assert 0 <= t;
      //@ assert t <= tagbuf_len;
      //      tag[t] = EOS;
      goto END;
    }
    if (__VERIFIER_nondet_int() != 0) {
      break;
    }
     //@ assert 0 <= t;
     //@ assert t <= tagbuf_len;
    t++;
  }

   //@ assert 0 <= t;
   //@ assert t <= tagbuf_len;
  t++;

  //@ loop_invariant 0 <= t && t <= tagbuf_len;
  while (1 == 1) {

    if (t == tagbuf_len) { /* Suppose t == tagbuf_len - 1 */
      //@ assert 0 <= t;
      //@ assert t <= tagbuf_len;
      goto END;
    }

    if (__VERIFIER_nondet_int() != 0) {
      if ( __VERIFIER_nondet_int() != 0) {
         //@ assert 0 <= t;
	//@ assert t <= tagbuf_len;
        t++;
        if (t == tagbuf_len) {
          //@ assert 0 <= t;
	  //@ assert t <= tagbuf_len;
          goto END;
        }
      }
    }
    else if ( __VERIFIER_nondet_int() != 0) {
      break;
    }

    /* OK */
    //@ assert 0 <= t;
    //@ assert t <= tagbuf_len;
    t++;                /* Now t == tagbuf_len + 1 
                         * So the bounds check (t == tagbuf_len) will fail */
  }
  /* OK */ 
  //@ assert 0 <= t;
  //@ assert t <= tagbuf_len;

 END:
  return 0;
}
