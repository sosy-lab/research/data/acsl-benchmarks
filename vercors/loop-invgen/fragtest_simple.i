int __VERIFIER_nondet_int();
int main(){
  int i,pvlen ;
  int tmp___1 ;
  int k = 0;
  int n;
  i = 0;
  pvlen = __VERIFIER_nondet_int();
  //@ loop_invariant k == 0;
  while ( __VERIFIER_nondet_int() != 0 && i <= 1000000) {
    i = i + 1;
  }
  if (i > pvlen) {
    pvlen = i;
  }
  i = 0;
  //@ loop_invariant k == i && i >= 0;
  while ( __VERIFIER_nondet_int() != 0 && i <= 1000000) {
    tmp___1 = i;
    i = i + 1;
    k = k + 1;
  }
  int j = 0;
  n = i;
  //@ loop_invariant k == i && i >= 0 && i + j == n;
  while (1 == 1) {
    //@ assert k >= 0;
    k = k -1;
    i = i - 1;
    j = j + 1;
    if (j >= n) {
      break;
    }
  }
  return 0;
}
