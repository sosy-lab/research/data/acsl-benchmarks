int __VERIFIER_nondet_int();
int main()
{
    int scheme;
    int urilen,tokenlen;
    int cp,c;
    urilen = __VERIFIER_nondet_int();
    tokenlen = __VERIFIER_nondet_int();
    scheme = __VERIFIER_nondet_int();
    if (!(urilen <= 1000000 && urilen >= -1000000)) return 0;
    if (!(tokenlen <= 1000000 && tokenlen >= -1000000)) return 0;
    if (!(scheme <= 1000000 && scheme >= -1000000)) return 0;
    if(urilen>0); else goto END;
    if(tokenlen>0); else goto END;
    if(scheme >= 0 );else goto END;
    if (scheme == 0 || (urilen-1 < scheme)) {
        goto END;
    }
    cp = scheme;
    //@ assert cp - 1 < urilen;
    //@ assert 0 <= cp - 1;
    if (__VERIFIER_nondet_int() != 0) {
        //@ assert cp < urilen;
        //@ assert 0 <= cp;
        //@ loop_invariant 0 <= cp && cp < urilen && tokenlen > 0;
        while ( cp != urilen-1) {
            if(__VERIFIER_nondet_int() != 0) break;
            //@ assert cp < urilen;
            //@ assert 0 <= cp;
            ++cp;
        }
        //@ assert cp < urilen;
        //@ assert 0 <= cp;
        if (cp == urilen-1) goto END;
        //@ assert cp + 1 < urilen;
        //@ assert 0 <= cp + 1;
        if (cp+1 == urilen-1) goto END;
        ++cp;
        scheme = cp;
        if (__VERIFIER_nondet_int() != 0) {
            c = 0;
            //@ assert cp < urilen;
            //@ assert 0 <= cp;
            //@ loop_invariant 0 <= cp && cp < urilen && 0 <= c && c < tokenlen;
            while ( cp != urilen-1
                    && c < tokenlen - 1) {
                //@ assert cp < urilen;
                //@ assert 0 <= cp;
                if (__VERIFIER_nondet_int() != 0) {
                    ++c;
                    //@ assert c < tokenlen;
                    //@ assert 0 <= c;
                    //@ assert cp < urilen;
                    //@ assert 0 <= cp;
                }
                ++cp;
            }
            goto END;
        }
    }
END:
    return 0;
}
