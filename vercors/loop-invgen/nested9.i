int __VERIFIER_nondet_int();
int main() {
    int i,j,k,n,l,m;
    n = __VERIFIER_nondet_int();
    m = __VERIFIER_nondet_int();
    l = __VERIFIER_nondet_int();
    if (!(-1000000 < n && n < 1000000)) return 0;
    if (!(-1000000 < m && m < 1000000)) return 0;
    if (!(-1000000 < l && l < 1000000)) return 0;
    if(3*n<=m+l); else goto END;
    //@ loop_invariant 3 * n <= m + l && i >= 0 && (n < 0 || i <= n);
    for (i=0;i<n;i++)
        //@ loop_invariant 3 * n <= m + l && 0 <= i && i < n && 2 * i <= j && j <= 3 * i;
        for (j = 2*i;j<3*i;j++)
            //@ loop_invariant 3 * n <= m + l && 0 <= i && i < n && 2 * i <= j && j < 3 * i && i <= k && k <= j;
            for (k = i; k< j; k++) {
                //@ assert k - i <= 2 * n;
            }
END:
    return 0;
}
