/*
 * Variant: This one just blindly copies the input into buffer and writes '>''\0' at the end.
 */

#include "assert.h"


int main ()
{
  int in;
  int inlen = __VERIFIER_nondet_int();
  int bufferlen = __VERIFIER_nondet_int();
  int buf;
  int buflim;

  if(bufferlen >1);else goto END;
  if(inlen > 0);else goto END;
  if(bufferlen < inlen);else goto END;

  buf = 0;
  in = 0;
  buflim = bufferlen - 2;

  //@ loop_invariant 0 <= buf && buf == in && in <= buflim && buflim == bufferlen - 2 && bufferlen - 2 < inlen;
  while (__VERIFIER_nondet_int() != 0)
  {
    if (buf == buflim)
      break;
    //@ assert 0 <= buf;
    //@ assert buf < bufferlen; 
    buf++;
out:
    in++;
    //@ assert 0 <= in;
    //@ assert in < inlen;
  }

    //@ assert 0 <= buf;
    //@ assert buf < bufferlen;
    buf++;

  /* OK */
  //@ assert 0 <= buf;
  //6
  //@ assert buf < bufferlen;

  buf++;

 END:  return 0;
}
