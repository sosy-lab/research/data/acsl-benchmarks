int main() {
  unsigned int x = 0;
  unsigned int y = 0;

  //@ loop_invariant x <= 268435455; 
  while (x < 268435455) {
    y = 0;

    //@ loop_invariant x < 268435455;
    while (y < 10) {
      y++;
    }

    x++;
  }

  //@ assert x % 2 == 1;
}
