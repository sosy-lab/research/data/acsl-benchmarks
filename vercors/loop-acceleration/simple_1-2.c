int main() {
  unsigned int x = 0;

  //@ loop_invariant x % 2 == 0;
  while (x < 268435455) {
    x += 2;
  }

  //@ assert x % 2 == 0;
}
