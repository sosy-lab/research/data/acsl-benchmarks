extern unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int x = 1;
  unsigned int y = __VERIFIER_nondet_uint();

  if (!(y > 0)) return 0;

  //@ loop_invariant 1 == x && x <= y;
  while (x < y) {
    if (x < y / x) {
      x *= x;
    } else {
      x++;
    }
  }

  //@ assert x == y;
}
