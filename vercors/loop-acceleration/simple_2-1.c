extern unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int x = __VERIFIER_nondet_uint();

  while (x < 268435455) {
    x++;
  }

  //@ assert x >= 268435455;
}
