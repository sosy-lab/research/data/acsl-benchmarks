extern unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int x = 0;
  unsigned int y = __VERIFIER_nondet_uint();

  //@ loop_invariant x % 2 == y % 2 || (y % 2 == 1 && x <= 99);
  while (x < 99) {
    if (y % 2 == 0) {
      x += 2;
    } else {
      x++;
    }
  }

  //@ assert x % 2 == y % 2;
}
