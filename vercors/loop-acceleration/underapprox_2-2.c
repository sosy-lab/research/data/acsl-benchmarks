int main() {
  unsigned int x = 0;
  unsigned int y = 1;

  //@ loop_invariant x <= 6;
  while (x < 6) {
    x++;
    y *= 2;
  }

  //@ assert x == 6;
}
