int main() {
  unsigned int x = 10;

  //@ loop_invariant x % 2 == 0;
  while (x >= 10) {
    x += 2;
  }

  //@ assert x % 2 == 0;
}
