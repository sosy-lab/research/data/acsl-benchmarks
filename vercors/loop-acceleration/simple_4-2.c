int main() {
  unsigned int x = 268435440;

  //@ loop_invariant x % 2 == 0;
  while (x > 0) {
    x -= 2;
  }

  //@ assert x % 2 == 0;
}
