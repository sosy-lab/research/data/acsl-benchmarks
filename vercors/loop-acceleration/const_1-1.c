int main() {
  unsigned int x = 1;
  unsigned int y = 0;

  //@ loop_invariant x == 0 || y == 0;
  while (y < 1024) {
    x = 0;
    y++;
  }

  //@ assert x == 0;
}
