extern int __VERIFIER_nondet_int();

int main() {
  int A[1024];
  int i;

  for (i = 0; i < 1024; i++) {
    A[i] = __VERIFIER_nondet_int();
  }

  //@ loop_invariant i <= 1024 - 1;
  for (i = 0; A[i] != 0; i++) {
    if (i >= 1024-1) {
      break;
    }
  }

  //@ assert i <= 1024;
}
