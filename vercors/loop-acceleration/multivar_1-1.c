extern unsigned int __VERIFIER_nondet_uint();


int main() {
  unsigned int x = __VERIFIER_nondet_uint();
  unsigned int y = x;

  //@ loop_invariant x == y;
  while (x < 1024) {
    x++;
    y++;
  }

  //@ assert x == y;
}
