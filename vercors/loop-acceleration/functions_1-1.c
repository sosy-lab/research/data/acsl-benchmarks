/*@
requires z % 2 == 0 && z < 268435455;
ensures \result % 2 == 0;
@*/
unsigned int f(unsigned int z) {
  return z + 2;
}

int main() {
  unsigned int x = 0;

  //@ loop_invariant x % 2 == 0;
  while (x < 268435455) {
    x = f(x);
  }

  //@ assert x % 2 == 0;
}
