int main(){
	int x=0,y=50000,z=0;
	x=0;
	//@ loop_invariant z == 0 && (x < y && y == 50000 || 50000 <= x && x == y);
	while(x<1000000){
		if(x<50000)
			x++;
		else{
			x=x+2;
			y=y+2;
		}
	}
	//@ loop_invariant x == y && y >= z && z == 0;
	while(y>z){
		y--;
		x--;
	}
	//@ assert x % 2 == 0;
	return 0;
}
