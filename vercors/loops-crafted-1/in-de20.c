extern unsigned int __VERIFIER_nondet_uint();

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0, z;
  //@ loop_invariant x + y == n;
  while(x>0)
  {
    x--;
    y++;
  }

  z = y;
  //@ loop_invariant x + z == n;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ assert x == n;
  return 0;
}
