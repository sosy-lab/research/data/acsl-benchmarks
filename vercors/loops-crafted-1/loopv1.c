int __VERIFIER_nondet_int();

int main() {
  int SIZE = 50000001;
  int n,i,j;
  n = __VERIFIER_nondet_int();
  if (!(n <= SIZE)) return 0;
  i = 0; j=0;
  //@ loop_invariant i % 3 == 0;
  while(i<n){ 
 
    if(__VERIFIER_nondet_int() != 0)	  
      i = i + 6; 
    else
     i = i + 3;    
  }
  //@ assert i % 3 == 0;
  return 0;
}
