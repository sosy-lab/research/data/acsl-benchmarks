extern void abort();
void assume_abort_if_not(_Bool cond) {
  if(!cond) {abort();}
}
extern int __VERIFIER_nondet_int();

int main()
{
        int SIZE = 200000; 
	int last = __VERIFIER_nondet_int();
	assume_abort_if_not(last > 0);
	int a=0,b=0,c=0,st=0,d=0;
	//@ loop_invariant a == b && d == 0 && SIZE == 200000 && last > 0;
	while(1 == 1) {
		st=1;  
		//@ loop_invariant a == b && d == 0 && c <= SIZE && SIZE == 200000 && last > 0 && ((st == 0 && c > last) || (st == 1 && c <= last));
		for(c=0;c<SIZE;c++) {
			if (c>=last)  { st = 0; }
		}
		if(st==0 && c==last+1){
			a+=3; b+=3;}
		else { a+=2; b+=2; } 
		if(c==last && st==0) { 
			a = a+1;
		}
		else if(st==1 && last<SIZE) { 
			d++;
		}
		if(d == SIZE) {
			a = 0; 
			b = 1;
		}
			
               //@ assert a == b && c == SIZE;
	}
	return 0;
}
