int main()
{
	int x = 0;
	int y = 500000;
	//@ loop_invariant x < y && y == 500000 || 500000 <= x && x == y; 
	while(x < 1000000) {
		if (x < 500000) {
			x = x + 1;
		} else {
			x = x + 1;
			y = y + 1;
		}
	}
	//@ assert y == x;
	return 0;
}
