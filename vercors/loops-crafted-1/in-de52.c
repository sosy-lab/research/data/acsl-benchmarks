extern unsigned int __VERIFIER_nondet_uint();

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0, z;
  //@ loop_invariant x + y == n;
  while(x>0)
  {
    x--;
    y++;
  }

  z = y;
  //@ loop_invariant x + z == y;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ loop_invariant y + z == x;
  while(y>0)
  {
    y--;
    z++;
  }

  //@ loop_invariant x + y == z;
  while(x>0)
  {
    x--;
    y++;
  }

  //@ loop_invariant y == z;
  while(z>0)
  {
    y--;
    z--;
  }

  //@ assert y == 0;
  return 0;
}
