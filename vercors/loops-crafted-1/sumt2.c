unsigned int __VERIFIER_nondet_uint();
int main() {
  int SIZE = 20000001;
  unsigned int n,i,j,l=0;
  n = __VERIFIER_nondet_uint();
  if (!(n <= SIZE)) return 0;
  i = 0;
  j = 0;
  l=0;
  //@ loop_invariant i + j == l;
  while( l < n ) {
	
	  if(l%2 == 0)
	    i = i + 1;
	  else 
		  j = j+1;
    l = l+1;
  }
  //@ assert i + j == l;
  return 0;
}

