int main()
{
	int x=0,y=50000,z=0;
	x=0;
	//@ loop_invariant z == 0 && (x < y && y == 50000 || 50000 <= x && x == y && y <= 1000000);
	while(x<1000000){
		if(x<50000)
			x++;
		else{
			x++;
			y++;
		}
	}
	//@ loop_invariant z == 0 && x == y && y >= 0 && x % 2 == 0;
	while(y>0){
		y=y-2;
		x=x-2;
	}
	 //@ assert z == x;
	return 0;
}
