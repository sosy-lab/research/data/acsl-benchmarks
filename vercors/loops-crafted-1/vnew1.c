unsigned int __VERIFIER_nondet_uint();
int main() {
  int SIZE = 20000001;
  unsigned int n,i,k;
  n = __VERIFIER_nondet_uint();
  if (!(n <= SIZE)) return 0;
  k = n;
  i = 0;
  //@ loop_invariant 3 * k + i == 3 * n && i <= n + 2;
  while( i < n ) {
    k--;
    i = i + 3;
  }
  int j = 0;
  //@ loop_invariant k + j >= n / 3;
  while( j < n/3 ) {
    //@ assert k > 0;
    k--;
    j++;
  }
  return 0;
}
