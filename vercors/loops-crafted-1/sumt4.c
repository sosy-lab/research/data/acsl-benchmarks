unsigned int __VERIFIER_nondet_uint();
int main() {
  int SIZE = 20000001;
  unsigned int n=0,i=0,k=0,j=0,l=0;
  unsigned int v4=0;
  n = __VERIFIER_nondet_uint();
  if (!(n <= SIZE)) return 0;
  //@ loop_invariant i + j + k + v4 == l;
  while( l < n ) {
	
	  if(l%4 == 0)
	    v4 = v4 + 1;
	  else if(l%3 == 0)
	    i = i + 1;
	  else if(l%2 == 0) 
		  j = j+1;
	  else 
	    k = k+1;
    l = l+1;
  }
  //@ assert i + j + k + v4 == l;
  return 0;
}

