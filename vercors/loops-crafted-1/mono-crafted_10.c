int main()
{
	unsigned int x = 0;
	unsigned int y = 10000000;
	unsigned int z=5000000;
	//@ loop_invariant y == 10000000 && (x < z && z == 5000000 || 5000000 <= x && x == z);
	while(x<y){	
		if(x>=5000000)
			z++;
		x++;
	}
	//@ assert z == x;
	return 0;
}
