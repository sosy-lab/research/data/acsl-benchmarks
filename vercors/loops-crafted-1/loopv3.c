int __VERIFIER_nondet_int();

int main() {
  int SIZE = 50000001;
  int i,j;
  i = 0; j=0;
  //@ loop_invariant i % 4 == 0;
  while(i<SIZE){ 

    if(__VERIFIER_nondet_int() != 0)	  
      i = i + 8; 
    else
     i = i + 4;    
	  
  }
  j = i/4 ;
    //@ assert j * 4 == i;
  return 0;
}
