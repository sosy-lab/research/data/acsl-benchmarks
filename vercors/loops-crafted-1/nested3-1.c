int main()
{
  unsigned int x = 0;
  unsigned int y = 0;
  unsigned int z = 0;
  unsigned int w = 0;

  //@ loop_invariant x <= 268435455;
  while (x < 268435455) {
    y = 0;

    //@ loop_invariant x < 268435455 && y <= 268435455;
    while (y < 268435455) {
   	z =0;
   	//@ loop_invariant x < 268435455 && y < 268435455 && z <= 268435455;
	while (z <268435455) {
	  z++;
	}
	//@ assert z % 4 != 0;
	y++;
    }
    //@ assert y % 2 != 0;

    x++;
  }
  //@ assert x % 2 != 0;
  return 0;

}
