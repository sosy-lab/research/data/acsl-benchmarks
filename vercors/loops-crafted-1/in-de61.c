extern unsigned int __VERIFIER_nondet_uint();

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0, z;
  //@ loop_invariant x + y == n;
  while(x>0)
  {
    x--;
    y++;
  }

  z = y;
  //@ loop_invariant x + z == y && y == n;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ loop_invariant y + z == x && x == n;
  while(y>0)
  {
    y--;
    z++;
  }

  //@ loop_invariant x + y == z && z == n;
  while(x>0)
  {
    x--;
    y++;
  }

  //@ loop_invariant x + z == y && y == n;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ loop_invariant y + z == n;
  while(y>0)
  {
    y--;
    z++;
  }

  //@ assert z == n;
  return 0;
}
