int main(){
	int x=0,y=500000,z=0;
	x=0;
	//@ loop_invariant z == 0 && (x < y && y == 500000 || 500000 <= x && x == y && y <= 1000000);
	while(x<1000000){
		if(x<500000)
			x++;
		else{
			x++;
			y++;
		}
	}
	//@ loop_invariant x == y + z && y + z == 500000 + y / 2 && y >= 0 && y % 2 == 0;
	while(y>0){
		x--;
		z++;
		y=y-2;
	}
	//@ assert z % 2 == 0;
	//@ assert x % 2 == 0;
	return 0;
}
