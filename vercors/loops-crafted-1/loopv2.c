int __VERIFIER_nondet_int();
int __VERIFIER_nondet_int();
int main() {
  int SIZE = 50000001;
  int n,i,j,k;
  n = __VERIFIER_nondet_int();
  if (!(n <= SIZE)) return 0;
  i = 0; j=0;
  //@ loop_invariant i % 2 == 0;
  while(i<n){ 

    i = i + 4;
    j = i +2;    
  }
  k =i;
  //@ loop_invariant k % 2 == 0;
  while( (j%2) == 0){
   j-=4;
   k -=4; 
  }
  //@ assert k % 2 == 0;
  return 0;
}
