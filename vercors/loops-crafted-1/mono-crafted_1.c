int main()
{
	int x=0,y=50000,z=0;
	x=0;
	//@ loop_invariant z == 0 && (x < y && y == 50000 || 50000 <= x && x == y);
	while(x<1000000){
		if(x<50000)
			x++;
		else{
			x++;
			y++;
		}
	}
	//@ loop_invariant x == y && y >= z;
	while(y>z){
		y--;
		x--;
	}
	//@ assert x == z;
	return 0;
}
