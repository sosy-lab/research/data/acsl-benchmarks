extern unsigned int __VERIFIER_nondet_uint();

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0, z;
  //@ loop_invariant x + y == n;
  while(x>0)
  {
    x--;
    y++;
  }

  z = y;
  //@ loop_invariant x + z == y;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ loop_invariant x == y;
  while(y>0)
  {
    x--;
    y--;
  }

  //@ assert x == 0;
  return 0;
}
