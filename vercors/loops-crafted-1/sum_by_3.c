unsigned int __VERIFIER_nondet_uint();
int main() {
  int SIZE = 20000001;
  unsigned int n,i,k;
  n = __VERIFIER_nondet_uint();
  if (!(n <= SIZE)) return 0;
  i = 0;
  //@ loop_invariant i <= n && n <= SIZE;
  while( i < n ) {
    i = i + 1;
  }
  int j = i;
  //@ loop_invariant i == j && j == n && n <= SIZE;
  while( j < n ) {
    j = j+1;
  }
  k = j;
  //@ loop_invariant i == j && j == k && k == n && n <= SIZE;
  while( k < n ) {
    k = k+1;
  }
  //@ assert (i + j + k) / 3 <= SIZE;
  return 0;
}

