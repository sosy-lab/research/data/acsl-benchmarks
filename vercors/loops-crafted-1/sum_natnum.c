int __VERIFIER_nondet_int();

int main() {
  int SIZE = 40000; 
  int i;
  unsigned long long sum;
  i = 0, sum =0; 
  //@ loop_invariant 2 * sum == i * (i + 1) && 0 <= i && i <= SIZE;
  while(i< SIZE){ 
      i = i + 1; 
      sum += i;
  }
  //@ assert sum == (SIZE * (SIZE + 1)) / 2;
  return 0;
}
