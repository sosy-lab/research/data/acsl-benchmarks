int main() {
unsigned int x = 0;
unsigned int y = 10000000;
unsigned int z=0;
        //@ loop_invariant z % 2 == 0 && z <= y && y == 10000000 && (0 == z && z <= x && x < 5000000 || z == 2 * (x - 5000000));
	while(x<y){	
		if(x>=5000000)
			z=z+2;
		x++;
	}
  //@ assert z % 2 == 0;
  return 0;
}
