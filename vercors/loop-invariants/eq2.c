extern unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int w = __VERIFIER_nondet_uint();
  unsigned int x = w;
  unsigned int y = w + 1;
  unsigned int z = x + 1;
  //@ loop_invariant y == z;
  while (__VERIFIER_nondet_uint() != 0) {
    y++;
    z++;
  }
  //@ assert y == z;
  return 0;
}
