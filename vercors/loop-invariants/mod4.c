extern int __VERIFIER_nondet_int();

int main() {
  unsigned int x = 0;
  //@ loop_invariant x % 4 == 0;
  while (__VERIFIER_nondet_int() != 0) {
    x += 4;
  }
  //@ assert x % 4 == 0;
  return 0;
}
