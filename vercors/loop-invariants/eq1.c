extern unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int w = __VERIFIER_nondet_uint();
  unsigned int x = w;
  unsigned int y = __VERIFIER_nondet_uint();
  unsigned int z = y;
  //@ loop_invariant (w == x) && (y == z);
  while (__VERIFIER_nondet_uint() != 0) {
    if (__VERIFIER_nondet_uint() != 0) {
      ++w; ++x;
    } else {
      --y; --z;
    }
  }
  //@ assert w == x && y == z;
  return 0;
}
