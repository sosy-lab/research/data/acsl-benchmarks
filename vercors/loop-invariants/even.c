extern int __VERIFIER_nondet_int();

int main() {
  unsigned int x = 0;
  //@ loop_invariant x % 2 == 0;
  while (__VERIFIER_nondet_int() != 0) {
    x += 2;
  }
  //@ assert x % 2 == 0;
  return 0;
}
