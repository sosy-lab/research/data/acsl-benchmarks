extern int __VERIFIER_nondet_int();

int main() {
  unsigned int x = 5;
  //@ loop_invariant (x & 5) == 5;
  while (__VERIFIER_nondet_int() != 0) {
    x += 8;
  }
  //@ assert (x & 5) == 5;
  return 0;
}
