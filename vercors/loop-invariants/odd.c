extern int __VERIFIER_nondet_int();

int main() {
  unsigned int x = 1;
  //@ loop_invariant x % 2 == 1;
  while (__VERIFIER_nondet_int() != 0) {
    x += 2;
  }
  //@ assert x % 2 == 1;
  return 0;
}
