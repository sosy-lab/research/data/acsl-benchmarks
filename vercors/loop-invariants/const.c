extern unsigned int __VERIFIER_nondet_uint();

int main() {
  unsigned int s = 0;
  //@ loop_invariant s == 0;
  while (__VERIFIER_nondet_uint() != 0) {
    if (s != 0) {
      ++s;
    }
    if (__VERIFIER_nondet_uint() != 0) {
      //@ assert s == 0;
    }
  }
  return 0;
}
