int __VERIFIER_nondet_int();
int main() {
    int i;
    //@ loop_invariant i <= 1000000;
    for (i = 0; i < 1000000; i++) ;
    //@ assert i == 1000000;
    return 0;
}
