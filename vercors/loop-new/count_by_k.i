int __VERIFIER_nondet_int();
int main() {
    int i;
    int k;
    k = __VERIFIER_nondet_int();
    if (!(0 <= k && k <= 10)) return 0;
    //@ loop_invariant i <= 1000000 * k && 0 <= k && k <= 10 && (i % k == 0 || k == 0);
    for (i = 0; i < 1000000*k; i += k) ;
    //@ assert i == 1000000 * k;
    return 0;
}
