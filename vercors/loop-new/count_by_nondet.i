int __VERIFIER_nondet_int();
int main() {
    int i = 0;
    int k = 0;
    //@ loop_invariant k <= 1000000 && k <= i;
    while(i < 1000000) {
        int j = __VERIFIER_nondet_int();
        if (!(1 <= j && j < 1000000)) return 0;
        i = i + j;
        k ++;
    }
    //@ assert k <= 1000000;
    return 0;
}
