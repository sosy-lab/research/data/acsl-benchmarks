#include "assert.h"

int main() {
    int i;
    //@ loop_invariant i <= 1000000 && i % 2 == 0;
    for (i = 0; i < 1000000; i += 2) ;
    //@ assert i == 1000000;
    return 0;
}
