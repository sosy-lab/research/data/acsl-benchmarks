int __VERIFIER_nondet_int();
int main() {
    int n = __VERIFIER_nondet_int();
    int m = __VERIFIER_nondet_int();
    int k = 0;
    int i,j;
    if (!(10 <= n && n <= 10000)) return 0;
    if (!(10 <= m && m <= 10000)) return 0;
    //@ loop_invariant 0 <= i && i <= n && n >= 10 && m >= 10 && k == m * i;
    for (i = 0; i < n; i++) {
 //@ loop_invariant 0 <= j && j <= m && 0 <= i && i < n && n >= 10 && m >= 10 && k == m * i + j;
 for (j = 0; j < m; j++) {
     k ++;
 }
    }
    //@ assert k >= 100;
    return 0;
}
