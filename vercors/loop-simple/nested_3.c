// This file is part of the SV-Benchmarks collection of verification tasks:
// https://github.com/sosy-lab/sv-benchmarks
//
// This file was part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

void reach_error();

int main() {
	int a = 6;
	int b = 6;
	int c = 6;


        //@ loop_invariant a <= b && b == c && c == 6;
	for(a = 0; a < 6; ++a) {
	        //@ loop_invariant b <= c && c == 6;
		for(b = 0; b < 6; ++b) {
		        //@ loop_invariant c <= 6;
			for(c = 0; c < 6; ++c) {

			}
		}
	}
	//@ assert a == b && b == c && c == 6;
	if(!(a == 6 && b == 6 && c == 6 )) {
		reach_error();
	}
	return 1;
}
