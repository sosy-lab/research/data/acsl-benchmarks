int __VERIFIER_nondet_int();

int main() {  
  char x[100], y[100];
  int i,j,k;
  k = __VERIFIER_nondet_int();
  
  i = 0;
  //@ loop_invariant x[i] == 0 && i == 0;
  while(x[i] != 0){
    y[i] = x[i];
    i++;
  }
  y[i] = 0;
  
  //@ assert k < 0 || k >= i || y[k] == 0;

  return 0;
}
