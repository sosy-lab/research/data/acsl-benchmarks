//@ ensures \result == 0;
int eval(int x) 
{
  while (1 == 1) {
      x=0;
      break;
  }
  return x;
}


int main() {
  int x=0;

  while(1 == 1)
  {
    x = eval(x);
    //@ assert x == 0;  
  }

  //@ assert x != 0;

  return 0;
}
