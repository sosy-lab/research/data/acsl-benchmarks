int main() {
  int i, sn=0;
  //@ loop_invariant sn == (i - 1) * 2 && i <= 8 + 1;
  for(i=1; i<=8; i++) {
    sn = sn + (2);
  }
  //@ assert sn == 8 * 2 || sn == 0;
}
