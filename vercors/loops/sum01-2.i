extern int __VERIFIER_nondet_int();
int main() {
  int i, n=__VERIFIER_nondet_int(), sn=0;
  if (!(n < 1000 && n >= -1000)) return 0;
  //@ loop_invariant (sn == 0 && n < i) || (sn == (i - 1) * 2 && i <= n + 1);
  for(i=1; i<=n; i++) {
    sn = sn + (2);
  }
  //@ assert sn == n * 2 || sn == 0;
}
