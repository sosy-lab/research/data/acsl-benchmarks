int main() {
  int x=0;

  //@ loop_invariant x == 0;
  while(1 == 1)
  {
    //@ assert x == 0;  
  }

  //@ assert x != 0;
}
