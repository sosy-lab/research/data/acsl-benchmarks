_Bool __VERIFIER_nondet_bool();
int __VERIFIER_nondet_int();

int foo(int x) {
  return x--;
}

int main() {
  int x=__VERIFIER_nondet_int();
  while (x > 0) {
    _Bool c = __VERIFIER_nondet_bool();
    if(c) x = foo(x);
    else x = foo(x);
  }
  //@ assert x <= 0;
}



