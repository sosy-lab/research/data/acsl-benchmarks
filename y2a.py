import sys
import yaml

from pathlib import Path

OUT_DIR = "out"


def get_program_from_path(program_path):
    if program_path.exists():
        with open(program_path) as f:
            program = f.read()
    else:
        path_parts = program_path.parts
        if not path_parts:
            print("Required field 'file_name' is missing or empty, skipping entry")
            return
        sv_benchmarks = Path("./c")
        if len(path_parts) >= 2:
            program_path = Path(sv_benchmarks, path_parts[-2], path_parts[-1])
        if program_path.exists():
            with open(program_path) as f:
                program = f.read()
        else:
            potential_paths = list(sv_benchmarks.glob("*/" + path_parts[-1]))
            if len(potential_paths) != 1:
                return
            with open(potential_paths[0]) as f:
                program = f.read()
    return program


def make_unique_name(path):
    out_file_name = Path(OUT_DIR, "annotated_" + path.name)
    if out_file_name.exists():
        i = 2
        base = out_file_name.stem
        out_file_name = Path(OUT_DIR, f"{base}.{i}{out_file_name.suffix}")
        while out_file_name.exists():
            i += 1
            out_file_name = Path(OUT_DIR, base + f".{i}" + out_file_name.suffix)
    return out_file_name


def add_annotations(to_add):
    for path in to_add:
        annotations = sorted(to_add[path], reverse=True)
        program = get_program_from_path(path)
        if not program:
            print(f"Unable to locate file {path}, skipping related entries")
            continue
        program = program.splitlines()
        for line, column, annotation in annotations:
            original = program[line - 1]
            program[line - 1] = original[column:]
            program.insert(line - 1, annotation)
            if original[:column].strip():
                program.insert(line - 1, original[:column])

        Path(OUT_DIR).mkdir(parents=True, exist_ok=True)
        out_file_name = make_unique_name(path)
        with open(out_file_name, "w") as f:
            f.write("\n".join(program))


def collect_annotations_from_witness(witness_file):
    with open(witness_file) as witness:
        witness_data = yaml.safe_load(witness)
    annotations_per_file = {}
    for entry in witness_data:
        program_path = Path(entry["location"]["file_name"])
        line = entry["location"]["line"]
        column = entry["location"]["column"]

        entry_type = entry["entry_type"]
        if entry_type == "loop_invariant":
            invariant = entry["loop_invariant"]["string"]
            annotation = "//@ loop invariant " + invariant
        elif entry_type == "location_invariant":
            invariant = entry["location_invariant"]["string"]
            annotation = "//@ assert " + invariant
        else:
            print(f"Unknown entry type {entry_type}, skipping entry")
            continue

        if program_path in annotations_per_file:
            annotations_per_file[program_path].append((line, column, annotation))
        else:
            annotations_per_file[program_path] = [(line, column, annotation)]
    return annotations_per_file


def main(options):
    if len(options) != 1:
        print("Unexpected number of arguments")
        sys.exit(1)
    witness_file = options[0]
    annotations = collect_annotations_from_witness(witness_file)
    add_annotations(annotations)


if __name__ == "__main__":
    main(sys.argv[1:])
