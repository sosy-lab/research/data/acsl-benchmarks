extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "underapprox_1-2.c", 3, "reach_error"); }

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}

int main(void) {
  unsigned int x = 0;
  unsigned int y = 1;

  //@ loop invariant (x == 0 && y == 1) || (x == 1 && y == 2) || (x == 2 && y == 4) || (x == 3 && y == 8) || (x == 4 && y == 16) || (x == 5 && y == 32) || (x == 6 && y == 64);
  while (x < 6) {
    x++;
    y *= 2;
  }

  //@ assert y % 3 != 0;
  __VERIFIER_assert(y % 3);
}
