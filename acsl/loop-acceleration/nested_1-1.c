extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "nested_1-1.c", 3, "reach_error"); }

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}

int main(void) {
  unsigned int x = 0;
  unsigned int y = 0;

  //@ loop invariant x <= 0x0fffffff; 
  while (x < 0x0fffffff) {
    y = 0;

    //@ loop invariant x < 0x0fffffff;
    while (y < 10) {
      y++;
    }

    x++;
  }

  //@ assert x % 2 == 1;
  __VERIFIER_assert(x % 2);
}
