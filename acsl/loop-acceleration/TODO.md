diamond_2-2:
Contains an unsigned underflow for x if y % 2 == 1. Although Frama-C does not regard unsigned underflow an error by default, the loop invariant can still not be proven.

overflow_1-1:
Contains an overflow that doesn't affect correctness but prevents Frama-C from proving the loop invariant.
