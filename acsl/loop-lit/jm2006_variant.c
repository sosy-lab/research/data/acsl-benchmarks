// Source: Credited to Anubhav Gupta
// appears in Ranjit Jhala, Ken McMillan: "A Practical and Complete Approach
// to Predicate Refinement", TACAS 2006

#include "assert.h"

int main() {
    int i, j;
    i = __VERIFIER_nondet_int();
    j = __VERIFIER_nondet_int();

    if (!(i >= 0 && i <= LARGE_INT)) return 0;
    if (!(j >= 0)) return 0;
    int x = i;
    int y = j;
    int z = 0;
    //@ loop invariant y + i - j == x - z;
    while(x != 0) {
        x --;
        y -= 2;
        z ++;
    }
    if (i == j) {
        //@ assert y == -z;
        __VERIFIER_assert(y == -z);
    }
    return 0;
}
