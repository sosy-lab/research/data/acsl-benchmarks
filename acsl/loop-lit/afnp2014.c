// Source: E. De Angelis, F. Fioravanti, J. A. Navas, M. Proietti:
// "Verification of Programs by Combining Iterated Specialization with
// Interpolation", HCVS 2014

#include "assert.h"

int main() {
    int x = 1;
    int y = 0;
    //@ loop invariant (x >= y >= 1 || (x == 1 && y == 0)) && y <= 1000;
    while (y < 1000 && __VERIFIER_nondet_int()) {
        x = x + y;
        y = y + 1;
    }
    //@ assert x >= y;
    __VERIFIER_assert(x >= y);
    return 0;
}
