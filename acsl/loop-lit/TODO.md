ddlm2013:
Contains a potential overflow that does not affect correctness, but Frama-C is unable to prove the loop invariant due to this.

