// Source: Sumit Gulwani, Nebosja Jojic: "Program Verification as
// Probabilistic Inference", POPL 2007.

#include "assert.h"

int main() {
    int x = 0;
    int y = 50;
    //@ loop invariant (x < y == 50 || x == y >= 50) && x <= 100;
    while(x < 100) {
	if (x < 50) {
	    x = x + 1;
	} else {
	    x = x + 1;
	    y = y + 1;
	}
    }
    //@ assert y == 100;
    __VERIFIER_assert(y == 100);
    return 0;
}
