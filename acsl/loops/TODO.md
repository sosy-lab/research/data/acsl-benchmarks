sum03-2:
Contains an overflow not affecting correctness that prevents Frama-C from proving the loop invariant/assertion.
