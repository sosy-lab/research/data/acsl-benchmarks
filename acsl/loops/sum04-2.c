extern void abort(void);
#include <assert.h>
void reach_error() { assert(0); }

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}
#define a (2)
#define SIZE 8
int main() { 
  int i, sn=0;
  //@ loop invariant sn == (i - 1) * a && i <= SIZE + 1;
  for(i=1; i<=SIZE; i++) {
    sn = sn + a;
  }
  //@ assert sn == SIZE * a || sn == 0;
  __VERIFIER_assert(sn==SIZE*a || sn == 0);
}

