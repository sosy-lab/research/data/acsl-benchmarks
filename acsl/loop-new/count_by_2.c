#include "assert.h"

int main() {
    int i;
    //@ loop invariant i <= LARGE_INT && i % 2 == 0;
    for (i = 0; i < LARGE_INT; i += 2) ;
    //@ assert i == LARGE_INT;
    __VERIFIER_assert(i == LARGE_INT);
    return 0;
}
