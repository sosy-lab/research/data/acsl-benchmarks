#include "assert.h"

int main() {
    int i;
    //@ loop invariant i <= LARGE_INT;
    for (i = 0; i != LARGE_INT; i++) {
        //@ assert i <= LARGE_INT;
	__VERIFIER_assert(i <= LARGE_INT);
    }
    return 0;
}
