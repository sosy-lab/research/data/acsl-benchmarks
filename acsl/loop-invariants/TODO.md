bin-suffix-5:
Frama-C can show that the loop invariant is established on loop entry, but not that the invariant is preserved. Probably due to lacking support for bit arithmetic?

even, mod4, odd:
These programs contain a potential overflow that does not affect correctness, but prevents Frama-C from proving the loop invariant.
