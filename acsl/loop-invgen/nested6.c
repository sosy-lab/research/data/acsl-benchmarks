#include "assert.h"

int main() {
    int i,j,k,n;

    k = __VERIFIER_nondet_int();
    n = __VERIFIER_nondet_int();
    if (!(n < LARGE_INT)) return 0;
    if( k == n) {
    } else {
        goto END;
    }

    //@ loop invariant k == n;
    for (i=0;i<n;i++) {
        //@ loop invariant k == n && j >= 2 * i;
        for (j=2*i;j<n;j++) {
            if( __VERIFIER_nondet_int() ) {
                //@ loop invariant 2 * i <= j <= k <= n;
                for (k=j;k<n;k++) {
                    //@ assert k >= 2 * i;
                    __VERIFIER_assert(k>=2*i);
                }
            }
            else {
                //@ assert k >= n;
                __VERIFIER_assert( k >= n );
                //@ assert k <= n;
                __VERIFIER_assert( k <= n );
            }
        }
    }
END:
    return 0;
}
