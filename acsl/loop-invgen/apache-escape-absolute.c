#include "assert.h"

int main()
{
    int scheme;
    int urilen,tokenlen;
    int cp,c;
    urilen = __VERIFIER_nondet_int();
    tokenlen = __VERIFIER_nondet_int();
    scheme = __VERIFIER_nondet_int();
    if (!(urilen <= LARGE_INT && urilen >= -LARGE_INT)) return 0;
    if (!(tokenlen <= LARGE_INT && tokenlen >= -LARGE_INT)) return 0;
    if (!(scheme <= LARGE_INT && scheme >= -LARGE_INT)) return 0;

    if(urilen>0); else goto END;
    if(tokenlen>0); else goto END;
    if(scheme >= 0 );else goto END;
    if (scheme == 0 || (urilen-1 < scheme)) {
        goto END;
    }

    cp = scheme;

    //@ assert cp - 1 < urilen;
    __VERIFIER_assert(cp-1 < urilen);
    //@ assert 0 <= cp - 1;
    __VERIFIER_assert(0 <= cp-1);

    if (__VERIFIER_nondet_int()) {
        //@ assert cp < urilen;
        __VERIFIER_assert(cp < urilen);
        //@ assert 0 <= cp;
        __VERIFIER_assert(0 <= cp);
        //@ loop invariant 0 <= cp < urilen && tokenlen > 0;
        while ( cp != urilen-1) {
            if(__VERIFIER_nondet_int()) break;
            //@ assert cp < urilen;
            __VERIFIER_assert(cp < urilen);
            //@ assert 0 <= cp;
            __VERIFIER_assert(0 <= cp);
            ++cp;
        }
        //@ assert cp < urilen;
        __VERIFIER_assert(cp < urilen);
        //@ assert 0 <= cp;
        __VERIFIER_assert( 0 <= cp );
        if (cp == urilen-1) goto END;
        //@ assert cp + 1 < urilen;
        __VERIFIER_assert(cp+1 < urilen);
        //@ assert 0 <= cp + 1;
        __VERIFIER_assert( 0 <= cp+1 );
        if (cp+1 == urilen-1) goto END;
        ++cp;

        scheme = cp;

        if (__VERIFIER_nondet_int()) {
            c = 0;
            //@ assert cp < urilen;
            __VERIFIER_assert(cp < urilen);
            //@ assert 0 <= cp;
            __VERIFIER_assert(0<=cp);
            //@ loop invariant 0 <= cp < urilen && 0 <= c < tokenlen;
            while ( cp != urilen-1
                    && c < tokenlen - 1) {
                //@ assert cp < urilen;
                __VERIFIER_assert(cp < urilen);
                //@ assert 0 <= cp;
                __VERIFIER_assert(0<=cp);
                if (__VERIFIER_nondet_int()) {
                    ++c;
                    //@ assert c < tokenlen;
                    __VERIFIER_assert(c < tokenlen);
                    //@ assert 0 <= c;
                    __VERIFIER_assert(0<=c);
                    //@ assert cp < urilen;
                    __VERIFIER_assert(cp < urilen);
                    //@ assert 0 <= cp;
                    __VERIFIER_assert(0<=cp);
                }
                ++cp;
            }
            goto END;
        }
    }

END:
    return 0;
}
