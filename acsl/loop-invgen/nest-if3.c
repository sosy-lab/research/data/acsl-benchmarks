#include "assert.h"
int main() {
  int i,k,n,l;

  n = __VERIFIER_nondet_int();
  l = __VERIFIER_nondet_int();
  if (!(l>0)) return 0;
  if (!(l < LARGE_INT)) return 0;
  if (!(n < LARGE_INT)) return 0;
  //@ loop invariant n <= k == 1 || (1 <= l < 1000000 + k && 1 <= k <= n < 1000000);
  for (k=1;k<n;k++){
    //@ loop invariant 1 <= l < 1000000 + k && 1 <= k < n < 1000000 && 1 <= l <= i && (i <= n || l > n);
    for (i=l;i<n;i++){  
      //@ assert 1 <= i;
      __VERIFIER_assert(1<=i);
    }
    if(__VERIFIER_nondet_int())
      l = l + 1;
  }
 }
