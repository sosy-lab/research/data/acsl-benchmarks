#include "assert.h"

int main() {
  int n;
  int i = 0;
  int k = 0;
  n = __VERIFIER_nondet_int();
  //@ loop invariant i == k && (0 <= i <= n || i == 0 > n);
  while( i < n ) {
	i++;
	k++;
  }
  int j = 0;
  //@ loop invariant k == j == 0 > n || (k + j == n >= 0 && j <= n);
  while( j < n ) {
    //@ assert k > 0;
    __VERIFIER_assert (k > 0);
    j++;
    k--;
  }
}
