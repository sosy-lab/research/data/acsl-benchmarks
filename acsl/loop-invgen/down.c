#include "assert.h"

int main() {
  int n;
  int k = 0;
  int i = 0;
  n = __VERIFIER_nondet_int();
  //@ loop invariant i == k && (i <= n || n < 0);
  while( i < n ) {
      i++;
      k++;
  }
  int j = n;
  //@ loop invariant k == j || j < 0;
  while( j > 0 ) {
      //@ assert k > 0;
      __VERIFIER_assert(k > 0);
      j--;
      k--;
  }
  return 0;
}
