#include "assert.h"

int main( int argc, char *argv[]){
  int n,l,r,i,j;

  n = __VERIFIER_nondet_int();
  if (!(1 <= n && n <= LARGE_INT)) return 0;


  l = n/2 + 1;
  r = n;
  if(l>1) {
    l--;
  } else {
    r--;
  }
  //@ loop invariant 1 <= l <= r <= n || r == 0;
  while(r > 1) {
    i = l;
    j = 2*l;
    //@ loop invariant 1 <= j && 1 <= i <= r && 1 <= l <= r <= n;
    while(j <= r) {
      if( j < r) {
        //@ assert 1 <= j;
	__VERIFIER_assert(1 <= j);
	//@ assert j <= n;
	__VERIFIER_assert(j <= n);
	//@ assert 1 <= j + 1;
	__VERIFIER_assert(1 <= j+1);
	//@ assert j + 1 <= n;
	__VERIFIER_assert(j+1 <= n);
	if( __VERIFIER_nondet_int() )
	  j = j + 1;
      }
      //@ assert 1 <= j;
      __VERIFIER_assert(1 <= j);
      //@ assert j <= n;
      __VERIFIER_assert(j <= n);
      if( __VERIFIER_nondet_int() ) { 
      	break;
      }
      //@ assert 1 <= i;
      __VERIFIER_assert(1 <= i);
      //@ assert i <= n;
      __VERIFIER_assert(i <= n);
      //@ assert 1 <= j;
      __VERIFIER_assert(1 <= j);
      //@ assert j <= n;
      __VERIFIER_assert(j <= n);
      i = j;
      j = 2*j;
    }
    if(l > 1) {
      //@ assert 1 <= l;
      __VERIFIER_assert(1 <= l);
      //@ assert l <= n;
      __VERIFIER_assert(l <= n);
      l--;
    } else {
      //@ assert 1 <= r;
      __VERIFIER_assert(1 <= r);
      //@ assert r <= n;
      __VERIFIER_assert(r <= n);
      r--;
    }
  }
  return 0;
}

