#include "assert.h"

int main() {
  int n0, n1;
  int i0 = 0;
  int k = 0;

  n0 = __VERIFIER_nondet_int();
  n1 = __VERIFIER_nondet_int();
  if (!(-LARGE_INT <= n0 && n0 < LARGE_INT)) return 0;
  if (!(-LARGE_INT <= n1 && n1 < LARGE_INT)) return 0;

  //@ loop invariant 0 <= k == i0 && (i0 <= n0 || n0 < 0);
  while( i0 < n0 ) {
    i0++;
    k++;
  }

  int i1 = 0;
  //@ loop invariant k == i0 + i1 && i0 >= 0 && i0 >= n0 && i1 >= 0 && (i1 <= n1 || n1 < 0);
  while( i1 < n1 ) {
    i1++;
    k++;
  }

  int j1 = 0;
  //@ loop invariant k + j1 == i0 + i1 && i0 >= n0 && i1 >= n1;
  while( j1 < n0 + n1 ) {
      //@ assert k > 0;
      __VERIFIER_assert(k > 0);
      j1++;
      k--;
  }
}
