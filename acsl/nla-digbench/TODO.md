cohencu / cohencu-ll:
Frama-C can not prove the third-last assertion, even though it is a mere tautology when replacing x, y, z with the formulas involving only n from the loop invariant.

cohendiv / cohendiv-ll:
Since Frama-C does not know the semantics of assume_abort_if_not (or rather of abort), it can not prove that the outer loop invariant is established. When asserting the condition via an if-statement verification succeeds.

dijkstra / dijkstra-u:
Need to specify that q is a power of 4 in the loop invariants -> how?

divbin2:
Similar problem as for djikstra, b is power of 2.

hard / hard2 / hard-ll:
Similar problem as for djikstra, d and p are powers of 2.

knuth:
Needs advanced modulo arithmetic, so most likely not possible to prove using Alt-Ergo.

lcm1 / lcm2 / prod4br / prod4br-ll:
Not clear why Frama-C can not prove some of these loop invariants. Might be a solver problem.

ps3 / ps3-ll / ps4 / ps4-ll / ps5 / ps5-ll / ps6 / ps6-ll:
Needs support for higher-order polynomials, so most likely not possible with Alt-Ergo.

prodbin / prodbin-ll:
Same problem as for cohendiv, Frama-C does not know the semantics of abort and therefore cannot prove that y >= 0 is established.
