extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "in-de52.c", 3, "reach_error"); }
extern unsigned int __VERIFIER_nondet_uint(void);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0, z;
  //@ loop invariant x + y == n;
  while(x>0)
  {
    x--;
    y++;
  }

  z = y;
  //@ loop invariant x + z == y;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ loop invariant y + z == x;
  while(y>0)
  {
    y--;
    z++;
  }

  //@ loop invariant x + y == z;
  while(x>0)
  {
    x--;
    y++;
  }

  //@ loop invariant y == z;
  while(z>0)
  {
    y--;
    z--;
  }

  //@ assert y == 0;
  __VERIFIER_assert(y==0);
  return 0;
}
