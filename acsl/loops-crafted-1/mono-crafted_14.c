extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "mono-crafted_14.c", 3, "reach_error"); }
void __VERIFIER_assert(int cond) { if(!(cond)) { ERROR: {reach_error();abort();} } }

int main(){
	int x=0,y=500000,z=0;
	x=0;
	//@ loop invariant z == 0 && (x < y == 500000 || 500000 <= x == y <= 1000000);
	while(x<1000000){
		if(x<500000)
			x++;
		else{
			x++;
			y++;
		}
	}
	//@ loop invariant x == y + z == 500000 + y / 2 && y >= 0 && y % 2 == 0;
	while(y>0){
		x--;
		z++;
		y=y-2;
	}
	//@ assert z % 2 == 0;
	__VERIFIER_assert(z%2==0);
	//@ assert x % 2 == 0;
	__VERIFIER_assert(x%2==0);
	return 0;
}
