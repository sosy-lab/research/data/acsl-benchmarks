iftelse:
Not sure why it can't be shown that the loop invariant is preserved... Frama-C can actually prove preservation if the conditions i % 2 == 0 and i % 2 != 0 are switched in the disjunction, but then the "invariant" is incorrect and never established.

in-de42:
Contains a potential overflow of z in the last loop, so Frama-C can not prove the corresponding loop invariant.

nested_delay_nd:
Frama-C has several problems with this program:
 - Since SIZE is a global variable, Frama-C can apparentely not ensure that the value is indeed always 200000. This is not a problem in similar programs, but it is not clear what the problem here is exactly (but it is alleviated by (re-)declaring SIZE in the main function).
 - Frama-C does not know what assume_abort_if_not does and therefore can not ensure that last > 0 indeed holds.
 - Furthermore, Frama-C can not ensure that the condition last > 0 is preserved by the call to __VERIFIER_assert. It is not clear why that is the case here, because Frama-C shows no such behavior on other programs.
