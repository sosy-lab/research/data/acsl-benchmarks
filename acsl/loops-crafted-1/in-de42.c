extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "in-de42.c", 3, "reach_error"); }
extern unsigned int __VERIFIER_nondet_uint(void);
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0, z;
  //@ loop invariant x + y == n;
  while(x>0)
  {
    x--;
    y++;
  }

  z = y;
  //@ loop invariant x + z == y == n;
  while(z>0)
  {
    x++;
    z--;
  }

  //@ loop invariant y + z == x == n;
  while(y>0)
  {
    y--;
    z++;
  }

  //@ loop invariant x + z == 2 * n;
  while(x>0)
  {
    x--;
    z++;
  }

  //@ assert z == 2 * n;
  __VERIFIER_assert(z==2*n);
  return 0;
}
