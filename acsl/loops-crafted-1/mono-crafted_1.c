extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "mono-crafted_1.c", 3, "reach_error"); }
void __VERIFIER_assert(int cond) { if(!(cond)) { ERROR: {reach_error();abort();} } }

int main()
{
	int x=0,y=50000,z=0;
	x=0;
	//@ loop invariant z == 0 && (x < y == 50000 || 50000 <= x == y);
	while(x<1000000){
		if(x<50000)
			x++;
		else{
			x++;
			y++;
		}
	}
	//@ loop invariant x == y >= z;
	while(y>z){
		y--;
		x--;
	}
	//@ assert x == z;
	__VERIFIER_assert(x==z);
	return 0;
}
