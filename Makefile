SHELL=/bin/bash -O extglob -c
ACSL_DIRS = $(shell find acsl -type d)
ACSL_FILES = $(shell find acsl -type f -name "*.[ci]")
VERCORS_DIRS = $(shell find vercors -type d)
VERCORS_FILES = $(shell find vercors -type f -name "*.[ci]")
PROGRAM_DIRS = $(shell find -L c -type d)
PROGRAM_FILES = $(shell find -L c -type f -name "*.[ci]")

.PHONY: diff

diff:
	cd acsl; for file in $$(ls */*.{c,i,yml}) ; do echo $$file; diff $$file ../c/$$(dirname $$file)/$$(basename $$file) ; echo "" ; done

patches: $(ACSL_DIRS) $(ACSL_FILES) $(PROGRAM_DIRS) $(PROGRAM_FILES)
	mkdir -p patches
	rm patches/* -rf
	git -C c describe --dirty > patches/sv-benchmarks-version.txt
	if [[ $$(cat patches/sv-benchmarks-version.txt) == *dirty ]]; then \
	echo; echo sv-benchmarks contains uncommited changes; exit 2; \
	fi;
	for file in $(ACSL_FILES) ;\
	do mkdir -p patches/$$(basename $$(dirname $$file)) ; \
	echo $$file; \
	if ! diff c/$$(basename $$(dirname $$file))/$$(basename $$file) $$file ; then \
	diff c/$$(basename $$(dirname $$file))/$$(basename $$file) $$file > patches/$$(basename $$(dirname $$file))/$$(basename $$file) ; \
	fi; echo; done;
	
witnesses: $(ACSL_DIRS) $(ACSL_FILES) $(PROGRAM_DIRS) $(PROGRAM_FILES)
	mkdir -p witnesses
	rm witnesses/* -rf
	for file in $(ACSL_FILES); do \
	mkdir -p witnesses/$$(basename $$(dirname $$file)); \
	echo $$file; \
	diff -u c/$$(basename $$(dirname $$file))/$$(basename $$file) acsl/$$(basename $$(dirname $$file))/$$(basename $$file) > tmp.patch; \
	python3 annotation2witness.py --lang ACSL tmp.patch witnesses/$$(basename $$(dirname $$file))/$$(basename $$file).yml; \
	done;
	rm tmp.patch

vercors-witnesses: $(VERCORS_DIRS) $(VERCORS_FILES) $(PROGRAM_DIRS) $(PROGRAM_FILES)
	mkdir -p vercors-witnesses
	rm vercors-witnesses/* -rf
	for file in $(VERCORS_FILES); do \
	mkdir -p vercors-witnesses/$$(basename $$(dirname $$file)); \
	echo $$file; \
	diff -u c/$$(basename $$(dirname $$file))/$$(basename $$file) vercors/$$(basename $$(dirname $$file))/$$(basename $$file) > tmp.patch; \
	python3 Annotation2Witness.py --lang VerCors tmp.patch vercors-witnesses/$$(basename $$(dirname $$file))/$$(basename $$file).yml; \
	done;
	rm tmp.patch
