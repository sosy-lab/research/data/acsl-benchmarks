# acsl-benchmarks

Annotated c programs from sv-benchmarks for experiments on how annotations can help various tools to solve these tasks.

For working with this repository, please link the c folder of sv-benchmarks to a folder with the same name (`c`)
in the top-level directory of this repository:

```
ln -s /path/to/sv-benchmarks/c /path/to/this/repo/c
```

# ACSL

Unless specified otherwise in the `TODO.md` in each subfolder of `acsl`, all annotations can be proven by the default config of Frama-C-WP, invoked via
```
frama-c[-gui] -wp <program>
```
In particular, this uses the default solver Alt-Ergo.

# VerCors

Annotated programs from the `acsl` directory translated into a format supported by VerCors.
More details in [vercors/README.md](vercors/README.md).

# Witnesses

ACSL annotations turned into invariant witness entries via `annotation2witness.py`. Script `y2a.py` translates witness entries into ACSL annotations.

# VerCors Witnesses

VerCors annotations turned into invariant witness entries via `annotation2witness.py`.