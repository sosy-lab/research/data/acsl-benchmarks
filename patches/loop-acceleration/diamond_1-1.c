16a17
>   //@ loop invariant x % 2 == y % 2 || (y % 2 == 1 && x <= 99);
24a26
>   //@ assert x % 2 == y % 2;
