16a17
>   //@ loop invariant x % 2 == y % 2 || (y % 2 == 1 && (x == 0 || x == (unsigned int) -5));
48a50
>   //@ assert x % 2 == y % 2;
