11a12,14
> /*@ behavior stays_even:
>       assumes z % 2 == 0 && z < 0x0fffffff;
>       ensures \result % 2 == 0; */
18a22
>   //@ loop invariant x % 2 == 0;
22a27
>   //@ assert x % 2 == 0;
