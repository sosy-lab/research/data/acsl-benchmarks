10a11
>     //@ loop invariant j == 10 - (i - 1)/2 && 6 <= j <= 10 && 1 <= i <= 9 && j % 3 == i % 3;
14a16
>     //@ assert j == 6;
