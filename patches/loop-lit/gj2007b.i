27a28
>     //@ loop invariant (0 <= m < x <= n) || (m == x == 0) || (n <= 0);
33a35
>     //@ assert m >= 0 || n <= 0;
34a37
>     //@ assert m < n || n <= 0;
