9a10
>     //@ loop invariant (0 <= m < x <= n) || (m == x == 0) || (n <= 0);
15a17
>     //@ assert m >= 0 || n <= 0;
16a19
>     //@ assert m < n || n <= 0;
