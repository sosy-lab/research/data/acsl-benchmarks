20a21
> 	//@ loop invariant a == b;
22a24
> 		//@ loop invariant a == b && c <= 200000 && (last != 200000 || st == 1);
29a32
> 		//@ assert a == b && c == 200000;
