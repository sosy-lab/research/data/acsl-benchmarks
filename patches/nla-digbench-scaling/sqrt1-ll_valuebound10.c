29a30
>     //@ loop invariant t == 2 * a + 1 && s == (a + 1) * (a + 1) && t * t - 4 * s + 2 * t + 1 == 0;
30a32
>         //@ assert t == 2*a + 1;
31a34
>         //@ assert s == (a + 1) * (a + 1);
32a36
> 	//@ assert t*t - 4*s + 2*t + 1 == 0;
43a48
>     //@ assert t == 2 * a + 1;
44a50
>     //@ assert s == (a + 1) * (a + 1);
45a52
>     //@ assert t*t - 4*s + 2*t + 1 == 0;
