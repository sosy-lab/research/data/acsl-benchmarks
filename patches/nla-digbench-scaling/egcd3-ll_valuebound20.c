34a35
>     //@ loop invariant a == y * r + x * p && b == x * q + y * s;
41a43
>         //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s;
48a51
>             //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s && v == b * d;
49a53
>                 //@ assert a == y * r + x * p;
50a55
>                 //@ assert b == x * q + y * s;
51a57
>                 //@ assert a == k * b + c;
52a59
>                 //@ assert v == b * d;
73a81
>     //@ assert p*x - q*x + r*y - s*y  == a;
