35a36
>     //@ loop invariant x * z - x - y + 1 == 0;
36a38
>         //@ assert x*z - x - y + 1 == 0;
49a52
>     //@ assert 1 + x - y == 0;
