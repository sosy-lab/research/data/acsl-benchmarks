33a34
>     //@ loop invariant z * x - x + a - az * y == 0;
34a36
>         //@ assert z*x - x + a - az*y == 0;
43a46
>     //@ assert z*x - x + a - az*y == 0;
