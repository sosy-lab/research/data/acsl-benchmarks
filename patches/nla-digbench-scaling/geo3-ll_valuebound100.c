35a36
>     //@ loop invariant z * x - x + a - az * y == 0;
36a38
>         //@ assert z*x - x + a - az*y == 0;
45a48
>     //@ assert z*x - x + a - az*y == 0;
