26a27
>     //@ loop invariant y * y - 2 * x + y == 0;
27a29
>         //@ assert (y * y) - 2 * x + y == 0;
36a39
>     //@ assert (y*y) - 2*x + y == 0;
