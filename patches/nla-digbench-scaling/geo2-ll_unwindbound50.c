33a34
>     //@ loop invariant 1 + x * z - x - z * y == 0;
34a36
>         //@ assert 1 + x*z - x - z*y == 0;
43a46
>     //@ assert 1 + x*z - x - z*y == 0;
