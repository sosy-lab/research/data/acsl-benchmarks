34a35
>     //@ loop invariant p * s - r * q == 1 && a == y * r + x * p && b == x * q + y * s;
35a37
>         //@ assert 1 == p * s - r * q;
36a39
>         //@ assert a == y * r + x * p;
37a41
>         //@ assert b == x * q + y * s;
53a58
>     //@ assert a - b == 0;
54a60
>     //@ assert p*x + r*y - b == 0;
55a62
>     //@ assert q*r - p*s + 1 == 0;
56a64
>     //@ assert q*x + s*y - b == 0;
