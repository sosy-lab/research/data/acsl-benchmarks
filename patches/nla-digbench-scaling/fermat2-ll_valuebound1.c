34a35
>     //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
35a37
>         //@ assert 4*(A+r) == u*u - v*v - 2*u + 2*v;
48a51
>     //@ assert ((long long) 4*A) == u*u - v*v  - 2*u + 2*v;
