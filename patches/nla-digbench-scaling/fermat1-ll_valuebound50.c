34a35
>     //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
35a37
>         //@ assert 4*(A+r) == u*u - v*v - 2*u + 2*v;
39a42
>         //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
40a44
> 	    //@ assert 4*(A+r) == u*u - v*v - 2*u + 2*v;
47a52
>         //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
48a54
> 	    //@ assert 4*(A+r) == u*u - v*v - 2*u + 2*v;
56a63
>     //@ assert ((long long) 4*A) == u*u - v*v - 2*u + 2*v;
