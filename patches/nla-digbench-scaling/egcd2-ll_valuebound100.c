40a41
>     //@ loop invariant a == y * r + x * p && b == x * q + y * s && q * xy + s * yy - q * x - b * y - s * y + b == 0 && xy == x * y && yy == y * y;
46a48
>         //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s && q * xy + s * yy - q * x - b * y - s * y + b == 0 && xy == x * y && yy == y * y;
47a50
>             //@ assert a == k * b + c;
48a52
>             //@ assert a == y*r + x*p;
49a54
>             //@ assert b == x * q + y * s;
50a56
> 	    //@ assert q*xy + s*yy - q*x - b*y - s*y + b == 0;
70a77
>     //@ assert q*x + s*y == 0;
71a79
>     //@ assert p*x + r*y == a;
