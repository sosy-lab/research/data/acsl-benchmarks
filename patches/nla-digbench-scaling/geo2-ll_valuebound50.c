34a35
>     //@ loop invariant 1 + x * z - x - z * y == 0;
35a37
>         //@ assert 1 + x*z - x - z*y == 0;
44a47
>     //@ assert 1 + x*z - x - z*y == 0;
