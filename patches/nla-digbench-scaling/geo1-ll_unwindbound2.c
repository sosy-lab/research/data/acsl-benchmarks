34a35
>     //@ loop invariant x * z - x - y + 1 == 0;
35a37
>         //@ assert x*z - x - y + 1 == 0;
48a51
>     //@ assert 1 + x - y == 0;
