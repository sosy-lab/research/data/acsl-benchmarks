13a14
>     //@ loop invariant k == n;
14a16
>         //@ loop invariant k == n && j >= 2 * i;
16a19
>                 //@ loop invariant 2 * i <= j <= k <= n;
17a21
>                     //@ assert k >= 2 * i;
21a26
>                 //@ assert k >= n;
22a28
>                 //@ assert k <= n;
