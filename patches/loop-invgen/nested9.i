33a34
>     //@ loop invariant 3 * n <= m + l && i >= 0 && (n < 0 || i <= n);
34a36
>         //@ loop invariant 3 * n <= m + l && 0 <= i < n && 2 * i <= j <= 3 * i;
35a38
>             //@ loop invariant 3 * n <= m + l && 0 <= i < n && 2 * i <= j < 3 * i && i <= k <= j;
36a40
>                 //@ assert k - i <= 2 * n;
