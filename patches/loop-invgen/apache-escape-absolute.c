23a24
>     //@ assert cp - 1 < urilen;
24a26
>     //@ assert 0 <= cp - 1;
27a30
>         //@ assert cp < urilen;
28a32
>         //@ assert 0 <= cp;
29a34
>         //@ loop invariant 0 <= cp < urilen && tokenlen > 0;
31a37
>             //@ assert cp < urilen;
32a39
>             //@ assert 0 <= cp;
35a43
>         //@ assert cp < urilen;
36a45
>         //@ assert 0 <= cp;
38a48
>         //@ assert cp + 1 < urilen;
39a50
>         //@ assert 0 <= cp + 1;
47a59
>             //@ assert cp < urilen;
48a61
>             //@ assert 0 <= cp;
49a63
>             //@ loop invariant 0 <= cp < urilen && 0 <= c < tokenlen;
51a66
>                 //@ assert cp < urilen;
52a68
>                 //@ assert 0 <= cp;
55a72
>                     //@ assert c < tokenlen;
56a74
>                     //@ assert 0 <= c;
57a76
>                     //@ assert cp < urilen;
58a78
>                     //@ assert 0 <= cp;
