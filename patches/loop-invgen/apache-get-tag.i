31a32
>   //@ loop invariant 0 <= t <= tagbuf_len;
33a35
>       //@ assert 0 <= t;
34a37
>       //@ assert t <= tagbuf_len;
40a44
>      //@ assert 0 <= t;
41a46
>      //@ assert t <= tagbuf_len;
44a50
>    //@ assert 0 <= t;
45a52
>    //@ assert t <= tagbuf_len;
47a55
>   //@ loop invariant 0 <= t <= tagbuf_len;
49a58
>       //@ assert 0 <= t;
50a60
>       //@ assert t <= tagbuf_len;
55a66
>   //@ assert 0 <= t;
56a68
>  //@ assert t <= tagbuf_len;
59a72
>    //@ assert 0 <= t;
60a74
>    //@ assert t <= tagbuf_len;
68a83
>     //@ assert 0 <= t;
69a85
>     //@ assert t <= tagbuf_len;
72a89
>   //@ assert 0 <= t;
73a91
>   //@ assert t <= tagbuf_len;
