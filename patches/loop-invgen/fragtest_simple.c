12a13
>   //@ loop invariant k == 0;
22a24
>   //@ loop invariant k == i >= 0;
30a33
>   //@ loop invariant k == i >= 0 && i + j == n;
32a36
>     //@ assert k >= 0;
