30a31
>   //@ loop invariant n <= k == 1 || (1 <= l < 1000000 + k && 1 <= k <= n < 1000000);
31a33
>     //@ loop invariant 1 <= l < 1000000 + k && 1 <= k < n < 1000000 && 1 <= l <= i && (i <= n || l > n);
32a35
>       //@ assert 1 <= i;
