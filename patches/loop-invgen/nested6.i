32a33
>     //@ loop invariant k == n;
33a35
>         //@ loop invariant k == n && j >= 2 * i;
35a38
>                 //@ loop invariant 2 * i <= j <= k <= n;
36a40
>                     //@ assert k >= 2 * i;
40a45
>                 //@ assert k >= n;
41a47
>                 //@ assert k <= n;
