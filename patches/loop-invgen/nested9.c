14a15
>     //@ loop invariant 3 * n <= m + l && i >= 0 && (n < 0 || i <= n);
15a17
>         //@ loop invariant 3 * n <= m + l && 0 <= i < n && 2 * i <= j <= 3 * i;
16a19
>             //@ loop invariant 3 * n <= m + l && 0 <= i < n && 2 * i <= j < 3 * i && i <= k <= j;
17a21
>                 //@ assert k - i <= 2 * n;
