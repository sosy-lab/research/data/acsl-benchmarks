34a35
>   //@ loop invariant 1 <= l <= r <= n || r == 0;
37a39
>     //@ loop invariant 1 <= j && 1 <= i <= r && 1 <= l <= r <= n;
39a42
>  //@ assert 1 <= j;
40a44
>  //@ assert j <= n;
41a46
>  //@ assert 1 <= j + 1;
42a48
>  //@ assert j + 1 <= n;
46a53
>       //@ assert 1 <= j;
47a55
>       //@ assert j <= n;
51a60
>       //@ assert 1 <= i;
52a62
>       //@ assert i <= n;
53a64
>       //@ assert 1 <= j;
54a66
>       //@ assert j <= n;
59a72
>       //@ assert 1 <= l;
60a74
>       //@ assert l <= n;
63a78
>       //@ assert 1 <= r;
64a80
>       //@ assert r <= n;
