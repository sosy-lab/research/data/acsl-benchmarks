13a14
>   //@ loop invariant 0 <= t <= tagbuf_len;
15a17
>       //@ assert 0 <= t;
16a19
>       //@ assert t <= tagbuf_len;
23a27
>      //@ assert 0 <= t;
24a29
>      //@ assert t <= tagbuf_len;
28a34
>    //@ assert 0 <= t;
29a36
>    //@ assert t <= tagbuf_len;
32a40
>   //@ loop invariant 0 <= t <= tagbuf_len;
35a44
>       //@ assert 0 <= t;
36a46
>       //@ assert t <= tagbuf_len;
42a53
>          //@ assert 0 <= t;
43a55
> 	//@ assert t <= tagbuf_len;
46a59
>           //@ assert 0 <= t;
47a61
> 	  //@ assert t <= tagbuf_len;
57a72
>     //@ assert 0 <= t;
58a74
>     //@ assert t <= tagbuf_len;
63a80
>   //@ assert 0 <= t;
64a82
>   //@ assert t <= tagbuf_len;
