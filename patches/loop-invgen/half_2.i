29a30
>   //@ loop invariant 2 * k + i == 2 * n && (i <= n + 1 || n < 0);
34a36
>   //@ loop invariant j >= 0 && (n - 1 <= 2 * j + 2 * k <= n || n < 0);
35a38
>     //@ assert k > 0; 
