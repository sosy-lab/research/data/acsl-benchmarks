15a16
>     //@ loop invariant limit == bufsize - 4 && (i == 0 > len || 0 <= i <= len);
16a18
>         //@ loop invariant limit == bufsize - 4 && 0 <= i <= len && (0 <= j <= limit + 2 == bufsize - 2 || j == 0 > limit);
18a21
>                 //@ assert i + 1 < len;
19a23
>                 //@ assert 0 <= i;
21a26
>                 //@ assert i < len;
22a28
>                 //@ assert 0 <= i;
23a30
>                 //@ assert j < bufsize;
24a32
>                 //@ assert 0 <= j;
28a37
>                 //@ assert i < len;
29a39
>                 //@ assert 0 <= i;
30a41
>                 //@ assert j < bufsize;
31a43
>                 //@ assert 0 <= j;
35a48
>                 //@ assert j < bufsize;
36a50
>                 //@ assert 0 <= j;
40a55
>                 //@ assert i < len;
41a57
>                 //@ assert 0 <= i;
42a59
>                 //@ assert j < bufsize;
43a61
>                 //@ assert 0 <= j;
