16a17
>   //@ loop invariant 1 <= l <= r <= n || r == 0;
19a21
>     //@ loop invariant 1 <= j && 1 <= i <= r && 1 <= l <= r <= n;
21a24
>         //@ assert 1 <= j;
22a26
> 	//@ assert j <= n;
23a28
> 	//@ assert 1 <= j + 1;
24a30
> 	//@ assert j + 1 <= n;
28a35
>       //@ assert 1 <= j;
29a37
>       //@ assert j <= n;
33a42
>       //@ assert 1 <= i;
34a44
>       //@ assert i <= n;
35a46
>       //@ assert 1 <= j;
36a48
>       //@ assert j <= n;
41a54
>       //@ assert 1 <= l;
42a56
>       //@ assert l <= n;
45a60
>       //@ assert 1 <= r;
46a62
>       //@ assert r <= n;
