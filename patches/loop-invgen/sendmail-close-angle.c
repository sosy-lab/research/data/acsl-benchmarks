23a24
>   //@ loop invariant 0 <= buf == in <= buflim == bufferlen - 2 < inlen;
27a29
>     //@ assert 0 <= buf;
28a31
>     //@ assert buf < bufferlen;
32a36
>     //@ assert 0 <= in;
33a38
>     //@ assert in < inlen;
36a42
>     //@ assert 0 <= buf;
37a44
>     //@ assert buf < bufferlen;
41a49
>   //@ assert 0 <= buf;
42a51
>   //@ assert buf < bufferlen;
