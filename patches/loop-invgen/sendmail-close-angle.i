36a37
>   //@ loop invariant 0 <= buf == in <= buflim == bufferlen - 2 < inlen;
40a42
>     //@ assert 0 <= buf;
41a44
>     //@ assert buf < bufferlen;
45a49
>     //@ assert 0 <= in;
46a51
>     //@ assert in < inlen;
48a54
>     //@ assert 0 <= buf;
49a56
>     //@ assert buf < bufferlen;
51a59
>   //@ assert 0 <= buf;
52a61
>   //@ assert buf < bufferlen;
