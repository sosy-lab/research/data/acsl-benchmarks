30a31
>   //@ loop invariant k == 0;
37a39
>   //@ loop invariant k == i >= 0;
44a47
>   //@ loop invariant k == i >= 0 && i + j == n;
45a49
>     //@ assert k >= 0;
