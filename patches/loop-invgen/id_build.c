26a27
>   //@ loop invariant i >= 0;
27a29
>     //@ loop invariant 0 <= nlen - 1 - i < nlen;
28a31
>       //@ assert 0 <= nlen - 1 - i;
29a33
>       //@ assert nlen - 1 - i < nlen;
