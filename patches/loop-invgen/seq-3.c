12a13
>   //@ loop invariant 0 <= k == i0 && (i0 <= n0 || n0 < 0);
18a20
>   //@ loop invariant k == i0 + i1 && i0 >= 0 && i0 >= n0 && i1 >= 0 && (i1 <= n1 || n1 < 0);
24a27
>   //@ loop invariant k + j1 == i0 + i1 && i0 >= n0 && i1 >= n1;
25a29
>       //@ assert k > 0;
