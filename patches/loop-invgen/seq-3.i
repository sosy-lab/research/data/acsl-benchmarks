31a32
>   //@ loop invariant 0 <= k == i0 && (i0 <= n0 || n0 < 0);
36a38
>   //@ loop invariant k == i0 + i1 && i0 >= 0 && i0 >= n0 && i1 >= 0 && (i1 <= n1 || n1 < 0);
41a44
>   //@ loop invariant k + j1 == i0 + i1 && i0 >= n0 && i1 >= n1;
42a46
>       //@ assert k > 0;
