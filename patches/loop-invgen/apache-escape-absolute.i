41a42
>     //@ assert cp - 1 < urilen;
42a44
>     //@ assert 0 <= cp - 1;
44a47
>         //@ assert cp < urilen;
45a49
>         //@ assert 0 <= cp;
46a51
>         //@ loop invariant 0 <= cp < urilen && tokenlen > 0;
48a54
>             //@ assert cp < urilen;
49a56
>             //@ assert 0 <= cp;
52a60
>         //@ assert cp < urilen;
53a62
>         //@ assert 0 <= cp;
55a65
>         //@ assert cp + 1 < urilen;
56a67
>         //@ assert 0 <= cp + 1;
62a74
>             //@ assert cp < urilen;
63a76
>             //@ assert 0 <= cp;
64a78
>             //@ loop invariant 0 <= cp < urilen && 0 <= c < tokenlen;
66a81
>                 //@ assert cp < urilen;
67a83
>                 //@ assert 0 <= cp;
70a87
>                     //@ assert c < tokenlen;
71a89
>                     //@ assert 0 <= c;
72a91
>                     //@ assert cp < urilen;
73a93
>                     //@ assert 0 <= cp;
