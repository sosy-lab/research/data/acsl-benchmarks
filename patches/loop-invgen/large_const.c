14a15
>   //@ loop invariant 0 <= i <= n < 10 && i * c2 <= k <= i * c3 && c1 == 4000 && c2 == 2000 && c3 == 10000;
27a29
>   //@ loop invariant n * c2 - j <= k <= n * c3 - j && c2 == 2000 && c3 == 10000;
28a31
>     //@ assert k > 0;
