33a34
>   //@ loop invariant limit == bufsize - 4 && (i == 0 > len || 0 <= i <= len);
34a36
>     //@ loop invariant limit == bufsize - 4 && 0 <= i <= len && (0 <= j <= limit + 2 == bufsize - 2 || j == 0 > limit);
36a39
>  //@ assert i + 1 < len;
37a41
>  //@ assert 0 <= i;
39a44
>         //@ assert i < len;
40a46
>  //@ assert 0 <= i;
41a48
>         //@ assert j < bufsize;
42a50
>  //@ assert 0 <= j;
45a54
>         //@ assert i < len;
46a56
>  //@ assert 0 <= i;
47a58
>         //@ assert j < bufsize;
48a60
>  //@ assert 0 <= j;
51a64
>         //@ assert j < bufsize;
52a66
>  //@ assert 0 <= j;
56a71
>         //@ assert i < len;
57a73
>  //@ assert 0 <= i;
58a75
>         //@ assert j < bufsize;
59a77
>  //@ assert 0 <= j;
