33a34
>   //@ loop invariant 0 <= i <= n < 10 && i * c2 <= k <= i * c3 && c1 == 4000 && c2 == 2000 && c3 == 10000;
45a47
>   //@ loop invariant n * c2 - j <= k <= n * c3 - j && c2 == 2000 && c3 == 10000;
46a49
>     //@ assert k > 0;
