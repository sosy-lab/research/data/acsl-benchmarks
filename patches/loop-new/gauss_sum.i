28a29
>     //@ loop invariant 2 * sum == (i - 1) * i && i - 1 <= n;
31a33
>     //@ assert 2 * sum == n * (n + 1);
