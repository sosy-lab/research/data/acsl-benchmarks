7a8
>     //@ loop invariant 2 * sum == (i - 1) * i && i - 1 <= n;
10a12
>     //@ assert 2 * sum == n * (n + 1);
