28a29
>     //@ loop invariant i <= 1000000 * k && 0 <= k <= 10 && (i % k == 0 || k == 0);
29a31
>     //@ assert i == 1000000 * k;
