20a21
>         //@ loop invariant a <= b == c == 6;
21a23,24
> 	        /*@ loop assigns b, c;
> 	            loop invariant b <= c == 6; */
22a26,27
> 		        /*@ loop assigns c;
> 		            loop invariant c <= 6; */
27a33
> 	//@ assert a == b == c == 6;
