22a23
>         //@ loop invariant a <= b == c == d == e == f == 6;
23a25,26
> 		/*@ loop assigns b, c, d, e, f;
> 	            loop invariant b <= c == d == e == f == 6; */
24a28,29
> 			/*@ loop assigns c, d, e, f;
> 		            loop invariant c <= d == e == f == 6; */
25a31,32
> 				/*@ loop assigns d, e, f;
> 			            loop invariant d <= e == f == 6; */
26a34,35
> 					/*@ loop assigns e, f;
> 				            loop invariant e <= f == 6; */
27a37,38
> 					        /*@ loop assigns f;
> 					            loop invariant f <= 6; */
35a47
> 	//@ assert a == b == c == d == e == f == 6;
