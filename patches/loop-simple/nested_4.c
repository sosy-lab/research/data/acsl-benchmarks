21a22
>         //@ loop invariant a <= b == c == d == 6;
22a24,25
> 	        /*@ loop assigns b, c, d;
> 	            loop invariant b <= c == d == 6; */
23a27,28
> 		        /*@ loop assigns c, d;
> 		            loop invariant c <= d == 6; */
24a30,31
> 			        /*@ loop assigns d;
> 			            loop invariant d <= 6; */
30a38
> 	//@ assert a == b == c == d == 6;
