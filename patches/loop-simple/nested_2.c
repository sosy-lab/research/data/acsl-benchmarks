19a20
> 	//@ loop invariant a <= b == 6;
20a22,23
> 		/*@ loop assigns b;
> 		    loop invariant b <= 6; */
24a28
> 	//@ assert a == b == 6;
