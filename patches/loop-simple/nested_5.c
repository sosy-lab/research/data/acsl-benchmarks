22a23
>         //@ loop invariant a <= b == c == d == e == 6;
23a25,26
> 		/*@ loop assigns b, c, d, e;
> 	            loop invariant b <= c == d == e == 6; */
24a28,29
> 			/*@ loop assigns c, d, e;
> 		            loop invariant c <= d == e == 6; */
25a31,32
> 				/*@ loop assigns d, e;
> 			            loop invariant d <= e == 6; */
26a34,35
> 				        /*@ loop assigns e;
> 				            loop invariant e <= 6; */
33a43
> 	//@ assert a == b == c == d == e == 6;
