25a26
>     //@ loop invariant 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
26a28
>         //@ assert 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
35a38
>     //@ assert 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
