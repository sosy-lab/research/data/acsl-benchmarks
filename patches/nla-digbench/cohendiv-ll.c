35a36
>     //@ loop invariant b == y * a && x == q * y + r && y >= 1;
36a38
>         //@ assert b == y * a;
37a40
> 	//@ assert x == q * y + r;
44a48
> 	//@ loop invariant b == y * a && x == q * y + r && r >= 0 && y >= 1;
45a50
> 	    //@ assert b == y * a;
46a52
> 	    //@ assert x == q * y + r;
47a54
> 	    //@ assert r >= 0;
52a60
> 	    //@ assert r >= 2 * y * a;
61a70
>     //@ assert x == q * y + r;
