38a39
>     //@ loop invariant d * d * q - 2 * q * d - 4 * r * d + 4 * k * d + 8 * r == 8 * n && k * t == t * t && d * d * q - 2 * d * q - 4 * d * r + 4 * d * t + 4 * a * k - 4 * a * t - 8 * n + 8 * r == 0 && d * k - d * t - a * k + a * t == 0;
39a41
>         //@ assert d * d * q - 2 * q * d - 4 * r * d + 4 * k * d + 8 * r == 8 * n;
40a43
>         //@ assert k * t == t * t;
41a45
>         //@ assert d * d * q - 2 * d * q - 4 * d * r + 4 * d * t + 4 * a * k - 4 * a * t - 8 * n + 8 * r == 0;
42a47
>         //@ assert d * k - d * t - a * k + a * t == 0;
