32a33
>     //@ loop invariant a == y * r + x * p && b == x * q + y * s;
39a41
>         //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s;
46a49
>             //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s && v == b * d;
47a51
>                 //@ assert a == y * r + x * p;
48a53
>                 //@ assert b == x * q + y * s;
49a55
>                 //@ assert a == k * b + c;
50a57
>                 //@ assert v == b * d;
71a79
>     //@ assert p * x - q * x + r * y - s * y == a;
