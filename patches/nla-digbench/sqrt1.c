27a28
>     //@ loop invariant t == 2 * a + 1 && s == (a + 1) * (a + 1) && t * t - 4 * s + 2 * t + 1 == 0;
28a30
>         //@ assert t == 2 * a + 1;
29a32
>         //@ assert s == (a + 1) * (a + 1);
30a34
>         //@ assert t * t - 4 * s + 2 * t + 1 == 0;
41a46
>     //@ assert t == 2 * a + 1;
42a48
>     //@ assert s == (a + 1) * (a + 1);
43a50
>     //@ assert t * t - 4 * s + 2 * t + 1 == 0;
