37a38
>     //@ loop invariant x * u + y * v == a * b;
38a40
>         //@ assert x * u + y * v == a * b;
42a45
>         //@ loop invariant x * u + y * v == a * b;
43a47
> 	    //@ assert x * u + y * v == a * b;
50a55
>         //@ loop invariant x * u + y * v == a * b;
51a57
> 	    //@ assert x * u + y * v == a * b;
59a66
>     //@ assert u * y + v * y == a * b;
60a68
>     //@ assert x == y;
