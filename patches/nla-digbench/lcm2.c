34a35
>     //@ loop invariant x * u + y * v == 2 * a * b;
35a37
>         //@ assert x * u + y * v == 2 * a * b;
49a52
>     //@ assert x * u + y * v == 2 * a * b;
