25a26
>     //@ loop invariant y * y - 2 * x + y == 0;
26a28
>         //@ assert y * y - 2 * x + y == 0;
35a38
>     //@ assert y * y - 2 * x + y == 0;
