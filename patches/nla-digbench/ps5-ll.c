26a27
>     //@ loop invariant 6 * y * y * y * y * y + 15 * y * y * y * y + 10 * y * y * y - 30 * x - y == 0 && c == y && (c <= k || k < 0 == c);
27a29
>         //@ assert 6 * y * y * y * y * y + 15 * y * y * y * y + 10 * y * y * y - 30 * x - y == 0;
37a40
>     //@ assert 6 * y * y * y * y * y + 15 * y * y * y * y + 10 * y * y * y - 30 * x - y == 0;
38a42
>     //@ assert k * y == y * y;
