33a34
>     //@ loop invariant x * z - x - y + 1 == 0;
34a36
>         //@ assert x * z - x - y + 1 == 0;
47a50
>     //@ assert 1 + x - y == 0;
