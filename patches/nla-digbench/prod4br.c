31a32
>     //@ loop invariant q + a * b * p == x * y;
32a34
>         //@ assert q + a * b * p == x * y;
54a57
>     //@ assert q == x * y;
55a59
>     //@ assert a * b == 0;
