32a33
>     //@ loop invariant 1 + x * z - x - z * y == 0;
33a35
>         //@ assert 1 + x * z - x - z * y == 0;
42a45
>     //@ assert 1 + x * z - x - z * y == 0;
