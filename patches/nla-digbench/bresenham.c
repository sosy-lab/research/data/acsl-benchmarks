29a30
>     //@ loop invariant 2 * Y * x - 2 * X * y - X + 2 * Y - v == 0 && (x <= X + 1 || (X < 0 == x && v == 2 * Y - X));
30a32
>     	//@ assert 2 * Y * x - 2 * X * y - X + 2 * Y - v == 0;
43a46
>     //@ assert 2 * Y * x - 2 * x * y - X + 2 * Y - v + 2 * y == 0;
