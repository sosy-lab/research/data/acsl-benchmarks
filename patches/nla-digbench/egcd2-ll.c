38a39
>     //@ loop invariant a == y * r + x * p && b == x * q + y * s && q * xy + s * yy - q * x - b * y - s * y + b == 0 && xy == x * y && yy == y * y;
44a46
>         //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s && q * xy + s * yy - q * x - b * y - s * y + b == 0 && xy == x * y && yy == y * y;
45a48
>             //@ assert a == k * b + c;
46a50
>             //@ assert a == y * r + x * p;
47a52
>             //@ assert b == x * q + y * s;
48a54
>             //@ assert q * xy + s * yy - q * x - b * y - s * y + b == 0;
68a75
>     //@ assert q * x + s * y == 0;
69a77
>     //@ assert p * x + r * y == a;
