33a34
>     //@ loop invariant q == 0 && r == A && d == B * p;
34a36
>         //@ assert q == 0;
35a38
>         //@ assert r == A;
36a40
>         //@ assert d == B * p;
43a48
>     //@ loop invariant A == q * B + r && d == B * p;
44a50
>         //@ assert A == q * B + r;
45a52
>         //@ assert d == B * p;
57a65
>     //@ assert A == d * q + r;
58a67
>     //@ assert B == d;
