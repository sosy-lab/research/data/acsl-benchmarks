33a34
>     //@ loop invariant a == y * r + x * p && b == x * q + y * s && q * x * y + s * y * y - q * x - b * y - s * y + b == 0;
39a41
>         //@ loop invariant k * b + c == a == y * r + x * p && b == x * q + y * s && q * x * y + s * y * y - q * x - b * y - s * y + b == 0;
40a43
>             //@ assert a == k * b + c;
41a45
>             //@ assert a == y * r + x * p;
42a47
>             //@ assert b == x * q + y * s;
43a49
>             //@ assert q * x * y + s * y * y - q * x - b * y - s * y + b == 0;
63a70
>     //@ assert q * x + s * y == 0;
64a72
>     //@ assert p * x + r * y == a;
