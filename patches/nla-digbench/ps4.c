24a25
>     //@ loop invariant 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0 && c == y && (c <= k || k < 0 == c);
25a27
>         //@ assert 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0; 
34a37
>     //@ assert k * y - y * y == 0;
35a39
>     //@ assert 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0;
