26a27
>     //@ loop invariant -2 * y * y * y * y * y * y - 6 * y * y * y * y * y - 5 * y * y * y * y + y * y + 12 * x == 0 && c == y && (c <= k || k < 0 == c);
27a29
>     	//@ assert -2 * y * y * y * y * y * y - 6 * y * y * y * y * y - 5 * y * y * y * y + y * y + 12 * x == 0;
37a40
>     //@ assert -2 * y * y * y * y * y * y - 6 * y * y * y * y * y -5 * y * y * y * y + y * y + 12 * x == 0;
38a42
>     //@ assert k * y == y * y;
