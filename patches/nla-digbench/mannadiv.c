34a35
>     //@ loop invariant y1 * x2 + y2 + y3 == x1;
35a37
>         //@ assert y1 * x2 + y2 + y3 == x1;
48a51
>     //@ assert y1 * x2 + y2 == x1;
