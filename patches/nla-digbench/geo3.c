31a32
>     //@ loop invariant z * x - x + a - a * z * y == 0;
32a34
>         //@ assert z * x - x + a - a * z * y == 0;
41a44
>     //@ assert z * x - x + a - a * z * y == 0;
