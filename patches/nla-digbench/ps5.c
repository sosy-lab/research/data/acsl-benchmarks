24a25
>     //@ loop invariant 6 * y * y * y * y * y + 15 * y * y * y * y + 10 * y * y * y - 30 * x - y == 0 && c == y && (c <= k || k < 0 == c);
25a27
>         //@ assert 6 * y * y * y * y * y + 15 * y * y * y * y + 10 * y * y * y - 30 * x - y == 0;
35a38
>     //@ assert 6 * y * y * y * y * y + 15 * y * y * y * y + 10 * y * y * y - 30 * x - y == 0;
36a40
>     //@ assert k * y == y * y;
