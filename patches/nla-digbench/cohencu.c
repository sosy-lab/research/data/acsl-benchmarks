29a30
>     //@ loop invariant z == 6 * n + 6 && y == 3 * n * n + 3 * n + 1 && x == n * n * n && y * z - 18 * x - 12 * y + 2 * z - 6 == z * z - 12 * y - 6 * z + 12 == 0 && (n <= a + 1 || a < 0 == n);
30a32
>         //@ assert z == 6 * n + 6;
31a34
>         //@ assert y == 3 * n * n + 3 * n + 1;
32a36
>         //@ assert x == n * n * n;
33a38
>         //@ assert y * z - 18 * x - 12 * y + 2 * z - 6 == 0;
34a40
> 	//@ assert z * z - 12 * y - 6 * z + 12 == 0;
44a51
>     //@ assert z == 6 * n + 6;
45a53
>     //@ assert 6 * a * x - x * z + 12 * x == 0;
46a55
>     //@ assert a * z - 6 * a - 2 * y + 2 * z - 10 == 0;
47a57
>     //@ assert 2 * y * y - 3 * x * z - 18 * x - 10 * y + 3 * z - 10 == 0;
48a59
>     //@ assert z * z - 12 * y - 6 * z + 12 == 0;
49a61
>     //@ assert y * z - 18 * x - 12 * y + 2 * z - 6 == 0;
