32a33
>     //@ loop invariant B == 1 && q == 0 && r == A && (b == 1 || b % 2 == 0);
36a38
>     //@ loop invariant A == q * b + r && (b == 1 || b % 2 == 0);
37a40
>     	//@ assert A == q * b + r;
46a50
>     //@ assert A == q * b + r;
