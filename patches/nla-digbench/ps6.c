24a25
>     //@ loop invariant -2 * y * y * y * y * y * y - 6 * y * y * y * y * y - 5 * y * y * y * y + y * y + 12 * x == 0 && c == y && (c <= k || k < 0 == c);
25a27
>         //@ assert -2 * y * y * y * y * y * y - 6 * y * y * y * y * y - 5 * y * y * y * y + y * y + 12 * x == 0;
35a38
>     //@ assert -2 * y * y * y * y * y * y - 6 * y * y * y * y * y -5 * y * y * y * y + y * y + 12 * x == 0;
36a40
>     //@ assert k * y == y * y;
