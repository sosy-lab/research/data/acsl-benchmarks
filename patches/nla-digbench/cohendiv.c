34a35
>     //@ loop invariant b == y * a && x == q * y + r && y >= 1;
35a37
>     	//@ assert b == y * a;
36a39
> 	//@ assert x == q * y + r;
43a47
> 	//@ loop invariant b == y * a && x == q * y + r && r >= 0 && y >= 1;
44a49
> 	    //@ assert b == y * a;
45a51
> 	    //@ assert x == q * y + r;
46a53
> 	    //@ assert r >= 0;
51a59
> 	    //@ assert r >= 2 * y * a;
60a69
>     //@ assert x == q * y + r;
