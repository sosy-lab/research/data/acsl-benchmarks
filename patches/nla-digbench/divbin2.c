33a34
>     //@ loop invariant B == 1 && q == 0 && r == A && (b == 1 || b % 2 == 0);
38a40
>     //@ loop invariant A == q * b + r && (b == 1 || b % 2 == 0);
39a42
>     	//@ assert A == q * b + r;
50a54
>     //@ assert A == q * b + r;
