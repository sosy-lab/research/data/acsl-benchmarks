32a33
>     //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
33a35
>         //@ assert 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
46a49
>     //@ assert 4 * A == u * u - v * v - 2 * u + 2 * v;
