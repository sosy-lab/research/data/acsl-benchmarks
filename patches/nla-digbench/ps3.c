24a25
>     //@ loop invariant 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
25a27
>         //@ assert 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
34a37
>     //@ assert 6 * x - 2 * y * y * y - 3 * y * y - y == 0;
