32a33
>     //@ loop invariant z * x - x + a - az * y == 0;
33a35
>         //@ assert z * x - x + a - az * y == 0;
42a45
>     //@ assert z * x - x + a - az * y == 0;
