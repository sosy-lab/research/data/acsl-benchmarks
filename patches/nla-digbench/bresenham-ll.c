29a30
>     //@ loop invariant 2 * Y * x - 2 * X * y - X + 2 * Y - v == 0 && (x <= X + 1 || (X < 0 == x && v == 2 * Y - X));
32a34
>         //@ assert 2 * yx - 2 * xy - X + 2 * Y - v == 0;
47a50
>     //@ assert 2 * yx - 2 * xy - X + 2 * Y - v + 2 * y == 0;
