28a29
>     //@ loop invariant p == h == 0 && r == n < 4294967295 / 4 && \exists integer m; q == \pow(4, m);
36a38
>     //@ loop invariant r < 2 * p + q && p * p + r * q == n * q && h * h * h - 12 * h * n * q + 16 * n * p * q - h * q * q - 4 * p * q * q + 12 * h * q * r - 16 * p * q * r == 0 && h * h * n - 4 * h * n * p + 4 * n * n * q - n * q * q - h * h * r + 4 * h * p * r - 8 * n * q * r + q * q * r + 4 * q * r * r == 0 && h * h * p - 4 * h * n * q + 4 * n * p * q - p * q * q + 4 * h * q * r - 4 * p * q * r == 0 && p * p - n * q + q * r == 0 && \exists integer m; q == \pow(4, m);
37a40
>      	//@ assert r < 2 * p + q;
38a42
>         //@ assert p * p + r * q == n * q;
39a44
>         //@ assert h * h * h - 12 * h * n * q + 16 * n * p * q - h * q * q - 4 * p * q * q + 12 * h * q * r - 16 * p * q * r == 0;
40a46
>         //@ assert h * h * n - 4 * h * n * p + 4 * n * n * q - n * q * q - h * h * r + 4 * h * p * r - 8 * n * q * r + q * q * r + 4 * q * r * r == 0; 
41a48
>         //@ assert h * h * p - 4 * h * n * q + 4 * n * p * q - p * q * q + 4 * h * q * r - 4 * p * q * r == 0;
42a50
>         //@ assert p * p - n * q + q * r == 0;
55a64
>     //@ assert h * h * h - 12 * h * n + 16 * n * p + 12 * h * r - 16 * p * r - h - 4 * p == 0;
56a66
>     //@ assert p * p - n + r == 0;
57a68
>     //@ assert h * h * p - 4 * h * n + 4 * n * p + 4 * h * r - 4 * p * r - p == 0;
