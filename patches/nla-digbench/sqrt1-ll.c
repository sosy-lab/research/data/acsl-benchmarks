28a29
>     //@ loop invariant t == 2 * a + 1 && s == (a + 1) * (a + 1) && t * t - 4 * s + 2 * t + 1 == 0;
29a31
>         //@ assert t == 2 * a + 1;
30a33
>         //@ assert s == (a + 1) * (a + 1);
31a35
>         //@ assert t * t - 4 * s + 2 * t + 1 == 0;
42a47
>     //@ assert t == 2 * a + 1;
43a49
>     //@ assert s == (a + 1) * (a + 1);
44a51
>     //@ assert t * t - 4 * s + 2 * t + 1 == 0;
