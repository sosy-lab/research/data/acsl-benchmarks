24a25
>     //@ loop invariant y * y - 2 * x + y == 0;
25a27
>         //@ assert y * y - 2 * x + y == 0;
34a37
>     //@ assert y * y - 2 * x + y == 0;
