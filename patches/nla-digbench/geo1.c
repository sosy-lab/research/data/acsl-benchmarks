31a32
>     //@ loop invariant x * z - x - y + 1 == 0;
32a34
>         //@ assert x * z - x - y + 1 == 0;
45a48
>     //@ assert 1 + x - y == 0;
