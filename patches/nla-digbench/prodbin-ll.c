31a32
>     //@ loop invariant z + x * y == a * b && y >= 0;
32a34
>         //@ assert z + x * y == a * b;
43a46
>     //@ assert z == a * b;
