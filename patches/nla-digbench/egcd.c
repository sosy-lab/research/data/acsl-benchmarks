32a33
>     //@ loop invariant p * s - r * q == 1 && a == y * r + x * p && b == x * q + y * s;
33a35
>         //@ assert 1 == p * s - r * q;
34a37
>         //@ assert a == y * r + x * p;
35a39
>         //@ assert b == x * q + y * s;
51a56
>     //@ assert a - b == 0;
52a58
>     //@ assert p * x + r * y - b == 0;    
53a60
>     //@ assert q * r - p * s + 1 == 0;
54a62
>     //@ assert q * x + s * y - b == 0;
