32a33
>     //@ loop invariant q == 0 && r == A && d == B * p;
33a35
>         //@ assert q == 0;
34a37
>         //@ assert r == A;
35a39
>         //@ assert d == B * p;
42a47
>     //@ loop invariant A == q * B + r && d == B * p;
43a49
>         //@ assert A == q * B + r;
44a51
>         //@ assert d == B * p;
56a64
>     //@ assert A == d * q + r;
57a66
>     //@ assert B == d;
