25a26
>     //@ loop invariant 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0 && c == y && (c <= k || k < 0 == c);
26a28
>         //@ assert 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0;
35a38
>     //@ assert k * y - y * y == 0;
36a40
>     //@ assert 4 * x - y * y * y * y - 2 * y * y * y - y * y == 0;
