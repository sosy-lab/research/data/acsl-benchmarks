31a32
>     //@ loop invariant z == 6 * n + 6 && y == 3 * n * n + 3 * n + 1 && x == n * n * n && y * z - 18 * x - 12 * y + 2 * z - 6 == z * z - 12 * y - 6 * z + 12 == 0 && (n <= a + 1 || a < 0 == n);
32a34
>         //@ assert z == 6 * n + 6;
33a36
>         //@ assert y == 3 * n * n + 3 * n + 1;
34a38
>         //@ assert x == n * n * n;
35a40
>         //@ assert y * z - 18 * x - 12 * y + 2 * z - 6 == 0;
36a42
> 	//@ assert z * z - 12 * y - 6 * z + 12 == 0;
46a53
>     //@ assert z == 6 * n + 6;
47a55
>     //@ assert 6 * a * x - x * z + 12 * x == 0;
48a57
>     //@ assert a * z - 6 * a - 2 * y + 2 * z - 10 == 0;
49a59
>     //@ assert 2 * y * y - 3 * x * z - 18 * x - 10 * y + 3 * z - 10 == 0;
50a61
>     //@ assert z * z - 12 * y - 6 * z + 12 == 0;
51a63
>     //@ assert y * z - 18 * x - 12 * y + 2 * z - 6 == 0;
