32a33
>     //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
33a35
>         //@ assert 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
37a40
>         //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
38a42
>             //@ assert 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
45a50
>         //@ loop invariant 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
46a52
>             //@ assert 4 * (A + r) == u * u - v * v - 2 * u + 2 * v;
54a61
>     //@ assert 4 * A == u * u - v * v - 2 * u + 2 * v;
