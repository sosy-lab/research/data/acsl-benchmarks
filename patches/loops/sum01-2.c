17a18
>   //@ loop invariant (sn == 0 && n < i) || (sn == (i - 1) * a && i <= n + 1);
20a22
>   //@ assert sn == n * a || sn == 0;
