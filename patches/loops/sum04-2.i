23a24
>   //@ loop invariant sn == (i - 1) * 2 && i <= 8 + 1;
26a28
>   //@ assert sn == 8 * 2 || sn == 0;
