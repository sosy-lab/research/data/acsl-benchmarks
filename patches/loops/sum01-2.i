24a25
>   //@ loop invariant (sn == 0 && n < i) || (sn == (i - 1) * 2 && i <= n + 1);
27a29
>   //@ assert sn == n * 2 || sn == 0;
