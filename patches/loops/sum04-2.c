14a15
>   //@ loop invariant sn == (i - 1) * a && i <= SIZE + 1;
17a19
>   //@ assert sn == SIZE * a || sn == 0;
