19a20
>   //@ loop invariant x1 >= 0 && x2 >= 0 && x3 >= 0;
28a30
>   //@ assert x1 == 0 || x2 == 0 || x3 == 0;
