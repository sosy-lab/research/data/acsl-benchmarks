20a21
>   //@ loop invariant i <= n <= SIZE;
24a26
>   //@ loop invariant i == j == n <= SIZE;
28a31
>   //@ loop invariant i == j == k == n <= SIZE;
31a35
>   //@ assert (i + j + k) / 3 <= SIZE;
