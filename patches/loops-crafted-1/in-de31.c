15a16
>   //@ loop invariant x + y == n;
22a24
>   //@ loop invariant x + z == y == n;
28a31
>   //@ loop invariant y + z == n;
34a38
>   //@ assert z == n;
