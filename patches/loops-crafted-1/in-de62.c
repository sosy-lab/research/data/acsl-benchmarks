15a16
>   //@ loop invariant x + y == n;
22a24
>   //@ loop invariant x + z == y;
28a31
>   //@ loop invariant y + z == x;
34a38
>   //@ loop invariant x + y == z;
40a45
>   //@ loop invariant x + z == y;
46a52
>   //@ loop invariant x == y;
52a59
>   //@ assert x == 0;
