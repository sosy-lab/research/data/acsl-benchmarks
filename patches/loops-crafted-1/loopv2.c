21a22
>   //@ loop invariant i % 2 == 0;
27a29
>   //@ loop invariant k % 2 == 0;
31a34
>   //@ assert k % 2 == 0;
