15a16
>   //@ loop invariant x + y == n;
22a24
>   //@ loop invariant x + z == y == n;
28a31
>   //@ loop invariant y + z == x == n;
34a38
>   //@ loop invariant x + y == z == n;
40a45
>   //@ loop invariant x + z == y == n;
46a52
>   //@ loop invariant y + z == n;
52a59
>   //@ assert z == n;
