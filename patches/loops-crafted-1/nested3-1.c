18a19
>   //@ loop invariant x <= 0x0fffffff;
21a23
>     //@ loop invariant x < 0x0fffffff && y <= 0x0fffffff;
23a26
>    	//@ loop invariant x < 0x0fffffff && y < 0x0fffffff && z <= 0x0fffffff;
26a30
> 	//@ assert z % 4 != 0;
29a34
>     //@ assert y % 2 != 0;
33a39
>   //@ assert x % 2 != 0;
