15a16
>   //@ loop invariant x + y == n;
22a24
>   //@ loop invariant x + z == y == n;
28a31
>   //@ loop invariant y + z == x == n;
34a38
>   //@ loop invariant x + z == 2 * n;
40a45
>   //@ assert z == 2 * n;
