9a10
> 	//@ loop invariant z == 0 && (x < y == 500000 || 500000 <= x == y <= 1000000);
17a19
> 	//@ loop invariant x == y + z && y >= 0 && y % 2 == 0;
22a25
> 	//@ assert x == z;
