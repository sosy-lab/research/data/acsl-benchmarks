8a9
> 	//@ loop invariant z == 0 && (x < y == 500000 || 500000 <= x == y <= 1000000);
16a18
> 	//@ loop invariant x == y + z == 500000 + y / 2 && y >= 0 && y % 2 == 0;
21a24
> 	//@ assert z % 2 == 0;
22a26
> 	//@ assert x % 2 == 0;
