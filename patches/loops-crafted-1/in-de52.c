15a16
>   //@ loop invariant x + y == n;
22a24
>   //@ loop invariant x + z == y;
28a31
>   //@ loop invariant y + z == x;
34a38
>   //@ loop invariant x + y == z;
40a45
>   //@ loop invariant y == z;
46a52
>   //@ assert y == 0;
