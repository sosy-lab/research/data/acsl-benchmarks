22a23
>   //@ loop invariant 2 * sum == i * (i + 1) && 0 <= i <= SIZE;
26a28
>   //@ assert sum == (SIZE * (SIZE + 1)) / 2;
