import argparse
import hashlib
import pycparser
import re
import uuid
import sys

from datetime import datetime
from enum import Enum
from operator import itemgetter
from pycparserext.ext_c_parser import GnuCParser
from typing import Optional
from unidiff import PatchSet

__version__ = "1.0"

PRODUCER_NAME = "Annotation2Witness"

AnnotationLanguage = Enum("AnnotationLanguage", ["ACSL", "VerCors"])
AnnotationType = Enum("AnnotationType", ["Assertion", "LoopInvariant"])


class FunctionCollector(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.functions = []

    def visit_FuncDef(self, node):
        self.functions.append((node.decl.name, node.decl.coord.line))


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("patch-file", help="The patch file containing the diff between the annotated"
                                           "and original program in unidiff format.")
    parser.add_argument("witness-file", help="File name of the witness that will be written.")
    parser.add_argument("-v", "--version", action="version", version=f"{PRODUCER_NAME} {__version__}",
                        help="Output version information and exit.")
    parser.add_argument("--lang", required=True, choices=[x.name for x in AnnotationLanguage],
                        help="The used annotation language.")

    parsed = parser.parse_args(args)
    parsed.lang = AnnotationLanguage[parsed.lang]
    return parsed


def remove_consecutive_comparisons(invariant: str) -> str:
    no_space = "".join(invariant.split())
    no_consecutive = []
    parts = re.split("([()])", no_space)
    for part in parts:
        predicates = re.split("(&&|\\|\\|)", part)
        for predicate in predicates:
            components = re.split("(<=|<|>=|>|==)", predicate)
            while len(components) > 3:
                no_consecutive.extend(components[:3])
                no_consecutive.append("&&")
                components = components[2:]
            no_consecutive.extend(components)
    return " ".join(no_consecutive)


def make_loop_invariant(program, invariant: str, line_number: int, function):
    with open("./loop_invariant_template.yaml") as f:
        template = f.read()
    with open(program, "rb") as f:
        programhash = hashlib.sha256(f.read()).hexdigest()
    template = re.sub(r"\$INPUT_FILE", program, template)
    template = re.sub(r"\$INPUT_HASH", programhash, template)
    template = re.sub(r"\$LOOP_INVARIANT", invariant, template)
    template = re.sub(r"\$LINE", str(line_number), template)
    template = re.sub(r"\$FUNCTION_NAME", function, template)
    template = re.sub(r"\$PRODUCER_NAME", PRODUCER_NAME, template)
    template = re.sub(r"\$PRODUCER_VERSION", __version__, template)
    template = re.sub(r"\$UUID", str(uuid.uuid4()), template)
    template = re.sub(r"\$CREATION_TIME", datetime.now().isoformat() + "Z", template)
    return template


def make_location_invariant(program, invariant: str, line_number: int, function):
    with open("./location_invariant_template.yaml") as f:
        template = f.read()
    with open(program, "rb") as f:
        programhash = hashlib.sha256(f.read()).hexdigest()
    template = re.sub(r"\$INPUT_FILE", program, template)
    template = re.sub(r"\$INPUT_HASH", programhash, template)
    template = re.sub(r"\$LOCATION_INVARIANT", invariant, template)
    template = re.sub(r"\$LINE", str(line_number), template)
    template = re.sub(r"\$FUNCTION_NAME", function, template)
    template = re.sub(r"\$PRODUCER_NAME", PRODUCER_NAME, template)
    template = re.sub(r"\$PRODUCER_VERSION", __version__, template)
    template = re.sub(r"\$UUID", str(uuid.uuid4()), template)
    template = re.sub(r"\$CREATION_TIME", datetime.now().isoformat() + "Z", template)
    return template


def get_annotation_type(annotation: str, lang: AnnotationLanguage) -> Optional[AnnotationType]:
    if lang == AnnotationLanguage.ACSL:
        if re.search(r"loop\s+invariant", annotation):
            return AnnotationType.LoopInvariant
        if re.search(r"assert", annotation):
            return AnnotationType.Assertion
    elif lang == AnnotationLanguage.VerCors:
        if re.search(r"loop_invariant", annotation):
            return AnnotationType.LoopInvariant
        if re.search(r"assert", annotation):
            return AnnotationType.Assertion
    else:
        raise AssertionError("Unhandled annotation language: " + lang)
    return None


def get_annotation_content(annotation: str, lang: AnnotationLanguage) -> Optional[str]:
    if lang == AnnotationLanguage.ACSL:
        annotation_content = re.search(r"(loop\s+invariant|assert)\s+(.*);", annotation)
        if annotation_content is not None:
            annotation_content = annotation_content.groups()[1]
            annotation_content = remove_consecutive_comparisons(annotation_content)
    elif lang == AnnotationLanguage.VerCors:
        annotation_content = re.search(r"(loop_invariant|assert)\s+(.*);", annotation)
        if annotation_content is not None:
            annotation_content = annotation_content.groups()[1]
    else:
        raise AssertionError("Unhandled annotation language: " + lang)
    return annotation_content


def get_current_function(functions, line_number):
    for function, function_start in sorted(functions, key=itemgetter(1), reverse=True):
        if function_start < line_number:
            break
    return function


def main(argv):
    args = parse_args(argv)
    patchset = PatchSet.from_filename(getattr(args, "patch-file"))
    patch = patchset[0]

    # Collect function definitions
    visitor = FunctionCollector()
    program = patch.source_file
    ast = pycparser.parse_file(program, use_cpp=True, cpp_args="-D__extension__=", parser=GnuCParser())
    visitor.visit(ast)
    if not visitor.functions:
        raise AssertionError(f"Failed to detect any function in source file {program}")

    # Line numbers in the patch also count added lines,
    # but the witness refers to line numbers in the unmodified program.
    # We therefore track how many add/remove lines we encountered
    # to compute the correct line number.
    added = 0
    witness = ""
    for hunk in patch:
        for line in hunk:
            if line.is_removed:
                added -= 1
            if line.is_added:
                line_number = line.target_line_no - added
                added += 1

                annotation_type = get_annotation_type(line.value, args.lang)
                if annotation_type is None:
                    print(f"Warning: Failed to determine annotation type for line:\n\t{line.value}")
                    continue

                annotation_content = get_annotation_content(line.value, args.lang)
                if annotation_content is None:
                    print(f"Warning: Failed to extract annotation from line:\n\t{line.value}")
                    continue

                function = get_current_function(visitor.functions, line_number)

                if annotation_type == AnnotationType.Assertion:
                    witness += make_location_invariant(program, annotation_content, line_number, function)
                elif annotation_type == AnnotationType.LoopInvariant:
                    witness += make_loop_invariant(program, annotation_content, line_number, function)
                else:
                    raise AssertionError("Unhandled annotation type: " + annotation_type)
    with open(getattr(args, "witness-file"), "w") as f:
        f.write(witness)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
