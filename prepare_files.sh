if [ $# -eq 0 ];
then echo "Specify target directory"; exit 1;
else target=$1;
fi


# Filter out buggy programs.
# Some programs are correct but are not part of the reach-safety track, so we only remove those that are definitely unsafe.
for file in $(ls $target | grep -P ".*?\.yml"); do
	delete=$(yq '.properties[] | select(.property_file == "../properties/unreach-call.prp") | .expected_verdict == false' <$target/$file)
	if [ "$delete" = "true" ];
	then rm "$target/${file%.*}"*;
	fi
done

# Create ACSL assertions from __VERIFIER_assert calls
for file in $(ls $target | grep -P ".*?\.[ci]"); do
	sed -r "s:(\s*)__VERIFIER_assert\((.*?)\);:\1//@ assert \2;\n&:" <$target/$file >$target/tmp.c
	mv $target/tmp.c $target/$file
done
